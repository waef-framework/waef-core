use quote::quote;
use syn::{parse_macro_input, parse_quote, DeriveInput, GenericParam, Generics};

#[proc_macro_derive(Pod)]
pub fn derive_pod(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree.
    let input = parse_macro_input!(input as DeriveInput);

    // Used in the quasi-quotation below as `#name`.
    let name = input.ident;
    let static_validation = match input.data {
        syn::Data::Struct(fields) => {
            fields.fields.into_iter().map(|field| {
               let field_type = field.ty;
               quote! {
                const _: fn() = || {
                    const _: fn() = || {
                        struct RequireFieldImplementsPod<T : waef_core::util::pod::Pod> { _phantom: std::marker::PhantomData<T> }
                        let _ = RequireFieldImplementsPod::<#field_type> { _phantom: Default::default() };
                    };
                };
            }
            }).collect()
        },
        syn::Data::Enum(_) => {
            vec![]
        },
        syn::Data::Union(_) => {
            vec![]
        },
    };

    // Add a bound `T: Pod` to every type parameter T.
    let generics = add_pod_trait_bounds(input.generics);

    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let expanded = quote! {
        #[automatically_derived]
        unsafe impl #impl_generics waef_core::util::pod::Pod for #name #ty_generics #where_clause { }

        #(#static_validation)*
    };

    // Hand the output tokens back to the compiler.
    proc_macro::TokenStream::from(expanded)
}

// Add a bound `T: Pod` to every type parameter T.
fn add_pod_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param
                .bounds
                .push(parse_quote!(waef_core::util::pod::Pod));
        }
    }
    generics
}

#[proc_macro_attribute]
pub fn component(
    component_name: proc_macro::TokenStream,
    target: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let pod_impl: proc_macro2::TokenStream = derive_pod(target.clone()).into();
    let component_name = parse_macro_input!(component_name as proc_macro2::TokenStream);
    let target_input = parse_macro_input!(target as DeriveInput);

    let name = target_input.ident.clone();
    let expanded = quote! {
        #[repr(C)]
        #[derive(Clone, Copy)]
        #target_input

        #pod_impl

        #[automatically_derived]
        impl ::waef_core::ecs::IsComponent for #name {
            fn component_name() -> &'static str {
                #component_name
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_attribute]
pub fn message_pod(
    message_name: proc_macro::TokenStream,
    target: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let pod_impl: proc_macro2::TokenStream = derive_pod(target.clone()).into();
    let message_name = parse_macro_input!(message_name as proc_macro2::TokenStream);
    let target_input = parse_macro_input!(target as DeriveInput);

    let name = target_input.ident.clone();
    let expanded = quote! {
        #[repr(C)]
        #[derive(Clone, Copy)]
        #target_input

        #pod_impl

        #[automatically_derived]
        impl ::waef_core::core::IsMessage for #name {
            fn into_message(self) -> waef_core::core::Message {
                ::waef_core::core::Message::new_from_pod(Self::category_name(), self)
            }

            fn from_message(msg: &::waef_core::core::Message) -> Option<Self>
            where
                Self: Sized {
                Some(::waef_core::util::pod::Pod::from_bytes(&msg.buffer))
            }

            fn category_name() -> &'static str {
                #message_name
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_attribute]
pub fn message_serde(
    message_name: proc_macro::TokenStream,
    target: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let message_name = parse_macro_input!(message_name as proc_macro2::TokenStream);
    let target_input = parse_macro_input!(target as DeriveInput);

    let name = target_input.ident.clone();
    let expanded = quote! {
        #target_input

        #[automatically_derived]
        impl ::waef_core::core::IsMessage for #name {
            fn into_message(self) -> waef_core::core::Message {
                ::waef_core::core::serde::serialize_message(&self).unwrap()
            }

            fn from_message(msg: &waef_core::core::Message) -> Option<Self>
            where
                Self: Sized {
                ::waef_core::core::serde::deserialize_message(msg).ok()
            }

            fn category_name() -> &'static str {
                #message_name
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}
