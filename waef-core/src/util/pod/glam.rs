use super::Pod;

unsafe impl Pod for glam::Vec2 {}
unsafe impl Pod for glam::Vec3 {}
unsafe impl Pod for glam::Vec4 {}
unsafe impl Pod for glam::Quat {}
unsafe impl Pod for glam::Affine2 {}
unsafe impl Pod for glam::Affine3A {}
unsafe impl Pod for glam::Mat2 {}
unsafe impl Pod for glam::Mat3 {}
unsafe impl Pod for glam::Mat3A {}
unsafe impl Pod for glam::Mat4 {}

unsafe impl Pod for glam::DVec2 {}
unsafe impl Pod for glam::DVec3 {}
unsafe impl Pod for glam::DVec4 {}
unsafe impl Pod for glam::DQuat {}
unsafe impl Pod for glam::DAffine2 {}
unsafe impl Pod for glam::DAffine3 {}
unsafe impl Pod for glam::DMat2 {}
unsafe impl Pod for glam::DMat3 {}
unsafe impl Pod for glam::DMat4 {}

unsafe impl Pod for glam::I16Vec2 {}
unsafe impl Pod for glam::I16Vec3 {}
unsafe impl Pod for glam::I16Vec4 {}
unsafe impl Pod for glam::U16Vec2 {}
unsafe impl Pod for glam::U16Vec3 {}
unsafe impl Pod for glam::U16Vec4 {}

unsafe impl Pod for glam::IVec2 {}
unsafe impl Pod for glam::IVec3 {}
unsafe impl Pod for glam::IVec4 {}
unsafe impl Pod for glam::UVec2 {}
unsafe impl Pod for glam::UVec3 {}
unsafe impl Pod for glam::UVec4 {}

unsafe impl Pod for glam::I64Vec2 {}
unsafe impl Pod for glam::I64Vec3 {}
unsafe impl Pod for glam::I64Vec4 {}
unsafe impl Pod for glam::U64Vec2 {}
unsafe impl Pod for glam::U64Vec3 {}
unsafe impl Pod for glam::U64Vec4 {}

unsafe impl Pod for glam::BVec2 {}
unsafe impl Pod for glam::BVec3 {}
unsafe impl Pod for glam::BVec4 {}
