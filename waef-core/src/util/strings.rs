pub fn read_utf8_null_terminated_string(data: &[u8]) -> String {
    if data.is_empty() {
        String::new()
    } else {
        let mut end = 0;
        while end < data.len() && data[end] != 0 {
            end += 1
        }
        match String::from_utf8(data[..end].to_vec()) {
            Ok(value) => value,
            Err(_) => String::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_utf8_null_terminated_string() {
        assert_eq!(
            "ABCDE",
            read_utf8_null_terminated_string(&[65, 66, 67, 68, 69, 0])
        );
        assert_eq!(
            "ABCDE",
            read_utf8_null_terminated_string(&[65, 66, 67, 68, 69])
        );

        assert_eq!(
            "ABC",
            read_utf8_null_terminated_string(&[65, 66, 67, 0, 68, 0])
        );
        assert_eq!("", read_utf8_null_terminated_string(&[0, 65, 66]));
        assert_eq!("", read_utf8_null_terminated_string(&[0]));
        assert_eq!("", read_utf8_null_terminated_string(&[]));
    }
}
