#[cfg(feature = "glam")]
pub mod glam;

#[inline]
fn is_aligned_for<Dst: Pod, Src: Pod>(src: &Src) -> bool {
    let ptr: *const Src = src;
    std::mem::align_of::<Src>() <= std::mem::align_of::<Dst>()
        || ptr as usize % std::mem::align_of::<Dst>() == 0
}

/// # Safety
///
/// Pod marks a class as available to be naively converted to and from byte representation, and between other POD representations,
/// and should be used on fixed-size types whose children are also all PODs.
pub unsafe trait Pod: Sized + Copy + 'static {
    #[inline]
    fn zeroed() -> Self {
        unsafe { std::mem::zeroed::<Self>() }
    }

    #[inline]
    fn byte_count() -> usize {
        std::mem::size_of::<Self>()
    }

    #[inline]
    fn cast_ptr_mut<T: Pod>(data: &mut [u8]) -> *mut T {
        if std::mem::size_of::<T>() == data.len() || data.len() % std::mem::size_of::<T>() == 0 {
            data as *mut _ as *mut T
        } else {
            std::ptr::null_mut()
        }
    }

    #[inline]
    fn try_split<T: Pod>(&self) -> &[T] {
        if is_aligned_for::<T, Self>(self) {
            unsafe {
                std::slice::from_raw_parts(
                    self as *const _ as *const T,
                    std::mem::size_of::<Self>() / std::mem::size_of::<T>(),
                )
            }
        } else {
            &[]
        }
    }

    #[inline]
    fn try_split_mut<T: Pod>(&mut self) -> &mut [T] {
        if is_aligned_for::<T, Self>(self) {
            unsafe {
                std::slice::from_raw_parts_mut(
                    self as *mut _ as *mut T,
                    std::mem::size_of::<Self>() / std::mem::size_of::<T>(),
                )
            }
        } else {
            &mut []
        }
    }

    #[inline]
    fn as_bytes(&self) -> &[u8] {
        self.try_split()
    }

    #[inline]
    fn as_bytes_mut(&mut self) -> &mut [u8] {
        self.try_split_mut()
    }

    #[inline]
    fn as_vec<T: Pod>(&self) -> Vec<T> {
        self.try_split::<T>().to_vec()
    }

    #[inline]
    fn into_vec<T: Pod>(self) -> Vec<T> {
        if is_aligned_for::<T, Self>(&self) {
            let bytes = self.try_split::<T>();
            Vec::from(bytes)
        } else {
            Vec::new()
        }
    }

    #[inline]
    fn into_boxed_slice<T: Pod>(self) -> Box<[T]> {
        if is_aligned_for::<T, Self>(&self) {
            let bytes = self.try_split::<T>();

            Vec::from(bytes).into_boxed_slice()
        } else {
            Box::new([])
        }
    }

    #[inline]
    fn into_bytes<const N: usize>(self) -> [u8; N] {
        let bytes = self.try_split::<u8>();
        if bytes.len() == N {
            let mut dst = unsafe { std::mem::zeroed::<[u8; N]>() };
            dst.copy_from_slice(bytes);
            dst
        } else {
            Pod::zeroed()
        }
    }

    #[inline]
    fn into_slice<T: Pod, const N: usize>(self) -> [T; N] {
        let bytes = self.try_split::<T>();
        if bytes.len() == N {
            let mut dst = unsafe { std::mem::zeroed::<[T; N]>() };
            dst.copy_from_slice(bytes);
            dst
        } else {
            Pod::zeroed()
        }
    }

    #[inline]
    fn copy(&self) -> Self {
        unsafe { std::ptr::read(self) }
    }

    #[inline]
    fn transmute_copy<T: Pod>(&self) -> T {
        if is_aligned_for::<T, Self>(self) {
            unsafe { std::mem::transmute_copy(self) }
        } else {
            T::zeroed()
        }
    }

    #[inline]
    fn from_bytes(src: &[u8]) -> Self {
        if src.len() != std::mem::size_of::<Self>() {
            Self::zeroed()
        } else {
            let mut me = Self::zeroed();
            unsafe {
                let underlying = std::slice::from_raw_parts_mut(
                    &mut me as *mut _ as *mut u8,
                    std::mem::size_of::<Self>(),
                );
                underlying.copy_from_slice(src)
            }
            me
        }
    }

    #[inline]
    fn try_from_bytes(src: &[u8]) -> Option<Self> {
        if src.len() != std::mem::size_of::<Self>() {
            None
        } else {
            let mut me = Self::zeroed();
            unsafe {
                let underlying = std::slice::from_raw_parts_mut(
                    &mut me as *mut _ as *mut u8,
                    std::mem::size_of::<Self>(),
                );
                underlying.copy_from_slice(src)
            }
            Some(me)
        }
    }

    #[inline]
    fn from_boxed_bytes(src: Box<[u8]>) -> Self {
        if src.len() != std::mem::size_of::<Self>() {
            Self::zeroed()
        } else {
            unsafe { *Box::from_raw(Box::into_raw(src) as *mut _ as *mut Self) }
        }
    }
}

unsafe impl Pod for u8 {}
unsafe impl Pod for i8 {}
unsafe impl Pod for u16 {}
unsafe impl Pod for i16 {}
unsafe impl Pod for u32 {}
unsafe impl Pod for i32 {}
unsafe impl Pod for u64 {}
unsafe impl Pod for i64 {}
unsafe impl Pod for u128 {}
unsafe impl Pod for i128 {}
unsafe impl Pod for usize {}
unsafe impl Pod for isize {}
unsafe impl Pod for f32 {}
unsafe impl Pod for f64 {}
unsafe impl Pod for bool {}
unsafe impl<T: Pod, const N: usize> Pod for [T; N] {}
unsafe impl<T: Pod> Pod for Option<T> {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_zeroed() {
        assert_eq!(0, i32::zeroed());
    }

    #[test]
    fn test_byte_count() {
        assert_eq!(4, i32::byte_count());
        assert_eq!(8, i64::byte_count());
    }

    #[quickcheck_macros::quickcheck]
    fn test_as_byte_conversion(src: i32) {
        let bytes = src.as_bytes();
        assert_eq!(4, bytes.len());
        assert_eq!(src, i32::from_bytes(bytes));

        let vecs = src.as_vec();
        assert_eq!(4, vecs.len());
        assert_eq!(src, i32::from_bytes(&vecs[0..4]));
    }

    #[quickcheck_macros::quickcheck]
    fn test_into_boxed_bytes_conversion(src: i32) {
        let bytes: Box<[u8]> = src.into_boxed_slice();
        assert_eq!(4, bytes.len());
        assert_eq!(src, i32::from_boxed_bytes(bytes));
    }

    #[quickcheck_macros::quickcheck]
    fn test_into_slice_conversion(src: i32) {
        let bytes: [u8; 4] = src.into_slice();
        assert_eq!(4, bytes.len());
        assert_eq!(src, i32::from_ne_bytes(bytes));
    }

    #[quickcheck_macros::quickcheck]
    fn test_into_bytes_conversion(src: i32) {
        let bytes: [u8; 4] = src.into_bytes();
        assert_eq!(4, bytes.len());
        assert_eq!(src, i32::from_ne_bytes(bytes));
    }

    #[quickcheck_macros::quickcheck]
    fn test_into_vec_conversion(src: i32) {
        let vecs = src.into_vec();
        assert_eq!(4, vecs.len());
        assert_eq!(src, i32::from_bytes(&vecs[0..4]));
    }

    #[quickcheck_macros::quickcheck]
    fn test_copy(mut src: i32) {
        let next = src.copy();
        assert_eq!(src, next);

        src = !src;
        assert_ne!(src, next);
    }

    #[quickcheck_macros::quickcheck]
    fn test_transumte(src: i32) {
        let next: [u16; 2] = src.transmute_copy();
        let back: i32 = next.transmute_copy();
        assert_eq!(src, back);
    }

    #[derive(Copy, Clone)]
    struct SampleTestPod {
        a: i32,
        b: u16,
    }
    unsafe impl Pod for SampleTestPod {}

    #[test]
    fn test_struct_as_pod() {
        let pod = SampleTestPod {
            a: 767902,
            b: 65500,
        };
        let byted: &[u8] = pod.try_split();
        let back = SampleTestPod::from_bytes(byted);
        assert!(byted.len() >= 6);
        assert_eq!(byted.len(), std::mem::size_of::<SampleTestPod>());
        assert_eq!(pod.a, back.a);
        assert_eq!(pod.b, back.b);
    }

    #[test]
    fn test_array_of_structs_to_array_of_bytes() {
        let pods: [SampleTestPod; 2] = [
            SampleTestPod {
                a: 767902,
                b: 65500,
            },
            SampleTestPod { a: 127902, b: 5500 },
        ];
        let bytes: &[u8] = pods.try_split();
        let back: [SampleTestPod; 2] = <[SampleTestPod; 2]>::from_bytes(bytes);
        assert_eq!(bytes.len(), std::mem::size_of::<SampleTestPod>() * 2);
        assert_eq!(pods[0].a, back[0].a);
        assert_eq!(pods[0].b, back[0].b);
        assert_eq!(pods[1].a, back[1].a);
        assert_eq!(pods[1].b, back[1].b);
    }

    #[test]
    fn test_ptr_casting() {
        let mut pods: [SampleTestPod; 2] = [
            SampleTestPod {
                a: 767902,
                b: 65500,
            },
            SampleTestPod { a: 127902, b: 5500 },
        ];
        let bytes: &mut [u8] = pods.try_split_mut();
        unsafe {
            let ptrs: *mut SampleTestPod = SampleTestPod::cast_ptr_mut(bytes);
            let back = std::slice::from_raw_parts_mut(ptrs, 2);
            pods[0].a = 54654;
            assert_eq!(54654, back[0].a);
            assert_eq!(65500, back[0].b);
            assert_eq!(127902, back[1].a);
            assert_eq!(5500, back[1].b);
        }
    }
}
