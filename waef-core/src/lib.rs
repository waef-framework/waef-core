pub mod actors;
pub mod core;
pub mod ecs;
pub mod resources;
pub mod timers;
pub mod util;

pub use actors::ActorAlignment;
pub use actors::HasActorAlignment;
pub use actors::IsActor;

pub use core::DispatchFilter;
pub use core::ExecutionResult;
pub use core::IsDispatcher;
pub use core::IsDisposable;
pub use core::IsMessage;
pub use core::IsTickable;
pub use core::Message;
pub use core::WaefError;

pub use ecs::extensions::EcsExtension;
pub use ecs::EcsRegistry;
pub use ecs::IsComponent;

pub use resources::extensions::ResourcesExtensions;
pub use resources::ResourceStore;

pub use timers::IsTimer;

pub use util::pod::Pod;
pub use waef_core_macros::Pod;

pub use waef_core_macros::component;
pub use waef_core_macros::message_pod;
pub use waef_core_macros::message_serde;
