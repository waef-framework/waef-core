use crate::ecs::IsComponent;
use std::marker::PhantomData;

pub struct Without<TComponent: IsComponent> {
    phantom: PhantomData<TComponent>,
}

pub trait ComponentQuery {
    fn get_component_names() -> Vec<String>;

    fn from_data(data: &mut [&mut [u8]]) -> Self;

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self;

    fn next_by() -> usize;
}

impl<TComponent: IsComponent> ComponentQuery for &mut TComponent {
    fn get_component_names() -> Vec<String> {
        vec![TComponent::component_name().into()]
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        unsafe { &mut *TComponent::cast_ptr_mut(data[0]) }
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        unsafe { &mut *(data[0].0 as *mut _ as *mut TComponent) }
    }

    fn next_by() -> usize {
        1
    }
}

impl<TComponent: IsComponent> ComponentQuery for &TComponent {
    fn get_component_names() -> Vec<String> {
        vec![TComponent::component_name().into()]
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        unsafe { &*TComponent::cast_ptr_mut(data[0]) }
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        unsafe { &*(data[0].0 as *const _ as *const TComponent) }
    }

    fn next_by() -> usize {
        1
    }
}

impl<TComponent: IsComponent> ComponentQuery for Option<&mut TComponent> {
    fn get_component_names() -> Vec<String> {
        vec![format!("?{}", TComponent::component_name())]
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        if data[0].iter().all(|e| *e == 0) {
            None
        } else {
            let output: &mut TComponent = unsafe { &mut *TComponent::cast_ptr_mut(data[0]) };
            Some(output)
        }
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        if data[0].0.is_null() {
            None
        } else {
            Some(unsafe { &mut *(data[0].0 as *mut _ as *mut TComponent) })
        }
    }

    fn next_by() -> usize {
        1
    }
}

impl<TComponent: IsComponent> ComponentQuery for Option<&TComponent> {
    fn get_component_names() -> Vec<String> {
        vec![format!("?{}", TComponent::component_name())]
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        if data[0].iter().all(|e| *e == 0) {
            None
        } else {
            let output: &TComponent = unsafe { &*TComponent::cast_ptr_mut(data[0]) };
            Some(output)
        }
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        if data[0].0.is_null() {
            None
        } else {
            Some(unsafe { &*(data[0].0 as *mut _ as *mut TComponent) })
        }
    }

    fn next_by() -> usize {
        1
    }
}

impl<TComponent: IsComponent> ComponentQuery for Without<TComponent> {
    fn get_component_names() -> Vec<String> {
        vec![format!("!{}", TComponent::component_name())]
    }

    fn from_data(_: &mut [&mut [u8]]) -> Self {
        Without::<TComponent> {
            phantom: PhantomData,
        }
    }

    fn from_ptr(_: &[(*mut u8, usize)]) -> Self {
        Without::<TComponent> {
            phantom: PhantomData,
        }
    }

    fn next_by() -> usize {
        0
    }
}

/*let results = "";

for (let i = 2; i < 25; ++i) {
    let component_index = Array(i).fill(0).map((_, index) => index);

    results += `impl<${component_index.map(i => `T${i} : ComponentQuery`).join(", ")}> ComponentQuery for (${component_index.map(i => `T${i}`).join(", ")})
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(${component_index.length});
        ${component_index.map(i => `result.extend(T${i}::get_component_names());`).join("\n        ")}
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            ${component_index.slice(1).map(i => `T${i}::from_data(&mut data[${component_index.slice(0, i).map(j => `T${j}::next_by()`).join(" + ")}..])`).join(",\n            ")}
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            ${component_index.slice(1).map(i => `T${i}::from_ptr(&data[${component_index.slice(0, i).map(j => `T${j}::next_by()`).join(" + ")}..])`).join(",\n            ")}
        )
    }

    fn next_by() -> usize {
        ${component_index.map(i => `T${i}::next_by()`).join(" + ")}
    }
}

`
}
results
*/
impl<T0: ComponentQuery, T1: ComponentQuery> ComponentQuery for (T0, T1) {
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(2);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (T0::from_ptr(data), T1::from_ptr(&data[T0::next_by()..]))
    }

    fn next_by() -> usize {
        T0::next_by() + T1::next_by()
    }
}

impl<T0: ComponentQuery, T1: ComponentQuery, T2: ComponentQuery> ComponentQuery for (T0, T1, T2) {
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(3);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
        )
    }

    fn next_by() -> usize {
        T0::next_by() + T1::next_by() + T2::next_by()
    }
}

impl<T0: ComponentQuery, T1: ComponentQuery, T2: ComponentQuery, T3: ComponentQuery> ComponentQuery
    for (T0, T1, T2, T3)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(4);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
        )
    }

    fn next_by() -> usize {
        T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(5);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
        )
    }

    fn next_by() -> usize {
        T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by() + T4::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(6);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(7);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(8);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7, T8)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(9);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7, T8, T9)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(10);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(11);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(12);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(13);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
    > ComponentQuery for (T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13)
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(14);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(15);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(16);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(17);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(18);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
        T18: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(19);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result.extend(T18::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
            + T18::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
        T18: ComponentQuery,
        T19: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(20);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result.extend(T18::get_component_names());
        result.extend(T19::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
            + T18::next_by()
            + T19::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
        T18: ComponentQuery,
        T19: ComponentQuery,
        T20: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(21);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result.extend(T18::get_component_names());
        result.extend(T19::get_component_names());
        result.extend(T20::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
            + T18::next_by()
            + T19::next_by()
            + T20::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
        T18: ComponentQuery,
        T19: ComponentQuery,
        T20: ComponentQuery,
        T21: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(22);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result.extend(T18::get_component_names());
        result.extend(T19::get_component_names());
        result.extend(T20::get_component_names());
        result.extend(T21::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
            T21::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
            T21::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
            + T18::next_by()
            + T19::next_by()
            + T20::next_by()
            + T21::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
        T18: ComponentQuery,
        T19: ComponentQuery,
        T20: ComponentQuery,
        T21: ComponentQuery,
        T22: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(23);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result.extend(T18::get_component_names());
        result.extend(T19::get_component_names());
        result.extend(T20::get_component_names());
        result.extend(T21::get_component_names());
        result.extend(T22::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
            T21::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()..],
            ),
            T22::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()
                    + T21::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
            T21::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()..],
            ),
            T22::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()
                    + T21::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
            + T18::next_by()
            + T19::next_by()
            + T20::next_by()
            + T21::next_by()
            + T22::next_by()
    }
}

impl<
        T0: ComponentQuery,
        T1: ComponentQuery,
        T2: ComponentQuery,
        T3: ComponentQuery,
        T4: ComponentQuery,
        T5: ComponentQuery,
        T6: ComponentQuery,
        T7: ComponentQuery,
        T8: ComponentQuery,
        T9: ComponentQuery,
        T10: ComponentQuery,
        T11: ComponentQuery,
        T12: ComponentQuery,
        T13: ComponentQuery,
        T14: ComponentQuery,
        T15: ComponentQuery,
        T16: ComponentQuery,
        T17: ComponentQuery,
        T18: ComponentQuery,
        T19: ComponentQuery,
        T20: ComponentQuery,
        T21: ComponentQuery,
        T22: ComponentQuery,
        T23: ComponentQuery,
    > ComponentQuery
    for (
        T0,
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
    )
{
    fn get_component_names() -> Vec<String> {
        let mut result = Vec::with_capacity(24);
        result.extend(T0::get_component_names());
        result.extend(T1::get_component_names());
        result.extend(T2::get_component_names());
        result.extend(T3::get_component_names());
        result.extend(T4::get_component_names());
        result.extend(T5::get_component_names());
        result.extend(T6::get_component_names());
        result.extend(T7::get_component_names());
        result.extend(T8::get_component_names());
        result.extend(T9::get_component_names());
        result.extend(T10::get_component_names());
        result.extend(T11::get_component_names());
        result.extend(T12::get_component_names());
        result.extend(T13::get_component_names());
        result.extend(T14::get_component_names());
        result.extend(T15::get_component_names());
        result.extend(T16::get_component_names());
        result.extend(T17::get_component_names());
        result.extend(T18::get_component_names());
        result.extend(T19::get_component_names());
        result.extend(T20::get_component_names());
        result.extend(T21::get_component_names());
        result.extend(T22::get_component_names());
        result.extend(T23::get_component_names());
        result
    }

    fn from_data(data: &mut [&mut [u8]]) -> Self {
        (
            T0::from_data(data),
            T1::from_data(&mut data[T0::next_by()..]),
            T2::from_data(&mut data[T0::next_by() + T1::next_by()..]),
            T3::from_data(&mut data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_data(
                &mut data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..],
            ),
            T5::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
            T21::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()..],
            ),
            T22::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()
                    + T21::next_by()..],
            ),
            T23::from_data(
                &mut data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()
                    + T21::next_by()
                    + T22::next_by()..],
            ),
        )
    }

    fn from_ptr(data: &[(*mut u8, usize)]) -> Self {
        (
            T0::from_ptr(data),
            T1::from_ptr(&data[T0::next_by()..]),
            T2::from_ptr(&data[T0::next_by() + T1::next_by()..]),
            T3::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by()..]),
            T4::from_ptr(&data[T0::next_by() + T1::next_by() + T2::next_by() + T3::next_by()..]),
            T5::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()..],
            ),
            T6::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()..],
            ),
            T7::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()..],
            ),
            T8::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()..],
            ),
            T9::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()..],
            ),
            T10::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()..],
            ),
            T11::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()..],
            ),
            T12::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()..],
            ),
            T13::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()..],
            ),
            T14::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()..],
            ),
            T15::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()..],
            ),
            T16::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()..],
            ),
            T17::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()..],
            ),
            T18::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()..],
            ),
            T19::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()..],
            ),
            T20::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()..],
            ),
            T21::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()..],
            ),
            T22::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()
                    + T21::next_by()..],
            ),
            T23::from_ptr(
                &data[T0::next_by()
                    + T1::next_by()
                    + T2::next_by()
                    + T3::next_by()
                    + T4::next_by()
                    + T5::next_by()
                    + T6::next_by()
                    + T7::next_by()
                    + T8::next_by()
                    + T9::next_by()
                    + T10::next_by()
                    + T11::next_by()
                    + T12::next_by()
                    + T13::next_by()
                    + T14::next_by()
                    + T15::next_by()
                    + T16::next_by()
                    + T17::next_by()
                    + T18::next_by()
                    + T19::next_by()
                    + T20::next_by()
                    + T21::next_by()
                    + T22::next_by()..],
            ),
        )
    }

    fn next_by() -> usize {
        T0::next_by()
            + T1::next_by()
            + T2::next_by()
            + T3::next_by()
            + T4::next_by()
            + T5::next_by()
            + T6::next_by()
            + T7::next_by()
            + T8::next_by()
            + T9::next_by()
            + T10::next_by()
            + T11::next_by()
            + T12::next_by()
            + T13::next_by()
            + T14::next_by()
            + T15::next_by()
            + T16::next_by()
            + T17::next_by()
            + T18::next_by()
            + T19::next_by()
            + T20::next_by()
            + T21::next_by()
            + T22::next_by()
            + T23::next_by()
    }
}
