use crate::{
    core::{IsMessage, Message},
    util::pod::Pod,
};

use super::IsComponent;

pub struct DetachComponent {
    pub entity_id: u128,
    pub component_name: String,
}
impl DetachComponent {
    pub fn new<TComponent: IsComponent>(entity_id: u128) -> Self {
        Self {
            entity_id,
            component_name: TComponent::component_name().into(),
        }
    }
}
impl IsMessage for DetachComponent {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(16 + self.component_name.len());
        data.extend(self.entity_id.try_split());
        data.extend(self.component_name.into_bytes());
        Message::new_from_boxed_bytes(Self::category_name(), data.into_boxed_slice())
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        if msg.buffer.len() <= 16 {
            None
        } else {
            let entity_id = u128::from_bytes(&msg.buffer[0..16]);
            let component_name = String::from_utf8_lossy(&msg.buffer[16..]).to_string();
            Some(Self {
                entity_id,
                component_name,
            })
        }
    }

    fn category_name() -> &'static str {
        "ecs:detach_component"
    }
}

pub struct AttachComponent {
    pub entity_id: u128,
    pub component_name: String,
    pub component_data: Box<[u8]>,
}
impl AttachComponent {
    pub fn new<TComponent: IsComponent>(entity_id: u128, data: TComponent) -> Self {
        Self {
            entity_id,
            component_name: TComponent::component_name().into(),
            component_data: data.into_boxed_slice(),
        }
    }
}

impl IsMessage for AttachComponent {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(16 + self.component_name.len());
        data.extend(self.entity_id.try_split());
        data.extend((self.component_name.len() as u32).try_split());
        data.extend(self.component_name.into_bytes());
        data.extend_from_slice(&self.component_data);
        Message::new_from_boxed_bytes(Self::category_name(), data.into_boxed_slice())
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        if msg.buffer.len() <= 16 {
            None
        } else {
            let entity_id = u128::from_bytes(&msg.buffer[0..16]);
            let component_name_len = u32::from_bytes(&msg.buffer[16..20]) as usize;
            let component_name =
                String::from_utf8_lossy(&msg.buffer[20..20 + component_name_len]).to_string();
            let component_data = msg.buffer[20 + component_name_len..]
                .to_vec()
                .into_boxed_slice();
            Some(Self {
                entity_id,
                component_name,
                component_data,
            })
        }
    }

    fn category_name() -> &'static str {
        "ecs:attach_component"
    }
}

#[derive(Copy, Clone)]
pub struct CreateEntity {
    pub entity_id: u128,
}
unsafe impl Pod for CreateEntity {}
impl IsMessage for CreateEntity {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "ecs:create_entity"
    }
}

pub struct CreateEntityWithComponents {
    pub entity_id: u128,
    pub components: Vec<(String, Vec<u8>)>,
}
impl IsMessage for CreateEntityWithComponents {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(128);
        data.extend(self.entity_id.try_split());
        data.extend((self.components.len() as u32).try_split());
        for (component_name, component_data) in self.components.into_iter() {
            data.extend((component_name.len() as u32).try_split());
            data.extend(component_name.as_bytes());
            data.extend((component_data.len() as u32).try_split());
            data.extend(component_data);
        }
        Message::new_from_boxed_bytes(Self::category_name(), data.into_boxed_slice())
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        let entity_id = u128::from_bytes(&msg.buffer[0..16]);
        let component_count = u32::from_bytes(&msg.buffer[16..20]) as usize;

        let mut components = Vec::new();
        let mut component_index = 20;
        for _ in 0..component_count {
            let name_len =
                u32::from_bytes(&msg.buffer[component_index..component_index + 4]) as usize;
            component_index += 4;

            let name =
                String::from_utf8_lossy(&msg.buffer[component_index..component_index + name_len]);
            component_index += name_len;

            let component_data_len =
                u32::from_bytes(&msg.buffer[component_index..component_index + 4]) as usize;
            component_index += 4;

            let component_data =
                msg.buffer[component_index..component_index + component_data_len].to_vec();
            component_index += component_data_len;

            components.push((name.to_string(), component_data));
        }

        Some(Self {
            entity_id,
            components,
        })
    }

    fn category_name() -> &'static str {
        "ecs:create_entity_with_components"
    }
}

#[derive(Copy, Clone)]
pub struct DestroyEntity {
    pub entity_id: u128,
}
unsafe impl Pod for DestroyEntity {}
impl IsMessage for DestroyEntity {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "ecs:destroy_entity"
    }
}
