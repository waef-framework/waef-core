use std::collections::HashMap;

use crate::IsComponent;

pub trait ComponentSet {
    fn to_hash_map(self) -> HashMap<String, Box<[u8]>>;
}
macro_rules! define_component_set_tuple_impl {
    ($($idx:tt $t:tt),+) => {
        impl<$($t,)+> ComponentSet for ($($t,)+)
        where
            $($t: IsComponent,)+
        {
            fn to_hash_map(self) -> HashMap<String, Box<[u8]>> {
                let mut result = HashMap::new();
                $(result.insert($t::component_name().to_string(), self.$idx.into_boxed_slice());)+
                result
            }
        }
    };
}

/* Generated via:
let result = ""
for (let i = 0; i < 25; ++i) {
    let builder = "impl_tuple!("
    for (let j = 0; j < i; ++j) {
        builder += `${j} T${j}`
        if (j !== i - 1) {
            builder += ", "
        }
    }
    builder += ");"
    result += builder + "\n"
}
result
*/

impl <T> ComponentSet for T
where
    T: IsComponent
{
    fn to_hash_map(self) -> HashMap<String, Box<[u8]>> {
        let mut result = HashMap::new();
        result.insert(Self::component_name().to_string(), self.into_boxed_slice());
        result
    }
}

define_component_set_tuple_impl!(0 T0, 1 T1);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17, 18 T18);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17, 18 T18, 19 T19);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17, 18 T18, 19 T19, 20 T20);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17, 18 T18, 19 T19, 20 T20, 21 T21);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17, 18 T18, 19 T19, 20 T20, 21 T21, 22 T22);
define_component_set_tuple_impl!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11, 12 T12, 13 T13, 14 T14, 15 T15, 16 T16, 17 T17, 18 T18, 19 T19, 20 T20, 21 T21, 22 T22, 23 T23);
