use crate::{
    core::{IsMessage, Message},
    util::pod::Pod,
};

#[derive(Copy, Clone)]
pub struct OnEntityCreated {
    pub entity_id: u128,
}
unsafe impl Pod for OnEntityCreated {}

impl IsMessage for OnEntityCreated {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "ecs:on_entity_created"
    }
}

#[derive(Copy, Clone)]
pub struct OnEntityDestroyed {
    pub entity_id: u128,
}
unsafe impl Pod for OnEntityDestroyed {}

impl IsMessage for OnEntityDestroyed {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "ecs:on_entity_destroyed"
    }
}

pub struct OnEntityComponentAttached {
    pub entity_id: u128,
    pub component_name: String,
}

impl IsMessage for OnEntityComponentAttached {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(16 + self.component_name.len());
        data.extend(self.entity_id.try_split());
        data.extend(self.component_name.as_bytes());
        Message::new_from_boxed_bytes(
            format!("{}:{}", Self::category_name(), self.component_name),
            data.into_boxed_slice(),
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        let entity_id = u128::from_bytes(&msg.buffer[0..16]);
        let component_name = String::from_utf8_lossy(&msg.buffer[16..]).into_owned();
        Some(Self {
            entity_id,
            component_name,
        })
    }

    fn category_name() -> &'static str {
        "ecs:on_entity_component_attached"
    }
}

pub struct OnEntityComponentDetached {
    pub entity_id: u128,
    pub component_name: String,
}

impl IsMessage for OnEntityComponentDetached {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(16 + self.component_name.len());
        data.extend(self.entity_id.try_split());
        data.extend(self.component_name.as_bytes());
        Message::new_from_boxed_bytes(
            format!("{}:{}", Self::category_name(), self.component_name),
            data.into_boxed_slice(),
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        let entity_id = u128::from_bytes(&msg.buffer[0..16]);
        let component_name = String::from_utf8_lossy(&msg.buffer[16..]).into_owned();
        Some(Self {
            entity_id,
            component_name,
        })
    }

    fn category_name() -> &'static str {
        "ecs:on_entity_component_detached"
    }
}

pub struct OnComponentDefined {
    pub component_size: u32,
    pub component_name: String,
}

impl IsMessage for OnComponentDefined {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(4 + self.component_name.len());
        data.extend(self.component_size.try_split());
        data.extend(self.component_name.as_bytes());
        Message::new_from_boxed_bytes(
            format!("{}:{}", Self::category_name(), self.component_name),
            data.into_boxed_slice(),
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        let component_size = u32::from_bytes(&msg.buffer[0..4]);
        let component_name = String::from_utf8_lossy(&msg.buffer[4..]).into_owned();
        Some(Self {
            component_size,
            component_name,
        })
    }

    fn category_name() -> &'static str {
        "ecs:on_component_defined"
    }
}
