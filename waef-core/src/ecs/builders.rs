use super::commands::CreateEntityWithComponents;
use crate::core::{IsMessage, Message, WaefError};
use crate::ecs::{EcsRegistry, IsComponent};
use crate::util::pod::Pod;
use std::collections::HashMap;
use std::sync::mpsc::Sender;
use std::sync::Arc;

pub trait ComponentBundle {
    fn build(self) -> Vec<(String, Box<[u8]>)>;
}

pub struct ComponentBundleBuilder {
    components: Vec<(String, Box<[u8]>)>,
}
pub struct ComponentBoxesBundle {
    components: Vec<(String, Box<[u8]>)>,
}
impl ComponentBundle for ComponentBoxesBundle {
    fn build(self) -> Vec<(String, Box<[u8]>)> {
        self.components
    }
}
impl ComponentBundleBuilder {
    pub fn new() -> Self {
        Self {
            components: Vec::new(),
        }
    }

    pub fn with_component<TComponent: IsComponent>(mut self, component: TComponent) -> Self {
        self.components.push((
            TComponent::component_name().into(),
            component.into_boxed_slice(),
        ));
        self
    }

    pub fn with_bundle<TBundle: ComponentBundle>(mut self, bundle: TBundle) -> Self {
        for item in bundle.build() {
            self.components.push(item)
        }
        self
    }

    pub fn to_bundle(self) -> ComponentBoxesBundle {
        ComponentBoxesBundle {
            components: self.components,
        }
    }
}

pub struct EntityBuilder {
    entity_id: u128,
    components: HashMap<String, Box<[u8]>>,
}

impl EntityBuilder {
    pub fn new(entity_id: u128) -> Self {
        Self {
            entity_id,
            components: HashMap::with_capacity(8),
        }
    }

    pub fn with_component<TComponent: IsComponent + Pod>(mut self, data: TComponent) -> Self {
        _ = self.components.insert(
            TComponent::component_name().into(),
            TComponent::into_boxed_slice(data),
        );
        self
    }

    pub fn try_with_component<TComponent: IsComponent + Pod>(
        mut self,
        data: Option<TComponent>,
    ) -> Self {
        if let Some(data) = data {
            _ = self.components.insert(
                TComponent::component_name().into(),
                TComponent::into_boxed_slice(data),
            );
        }
        self
    }
    pub fn with_bundle<TComponentBundle: ComponentBundle>(
        mut self,
        data: TComponentBundle,
    ) -> Self {
        for (name, data) in data.build() {
            _ = self.components.insert(name, data);
        }
        self
    }

    pub fn create(self, ecs: &Arc<dyn EcsRegistry>) -> Result<u128, WaefError> {
        ecs.create_entity_with_components(self.entity_id, self.components)?;
        Ok(self.entity_id)
    }

    pub fn send(self, sender: &Sender<Message>) -> u128 {
        let (entity_id, message) = self.into_message();
        _ = sender.send(message);
        entity_id
    }

    pub fn into_message(self) -> (u128, Message) {
        let output = CreateEntityWithComponents {
            entity_id: self.entity_id,
            components: self
                .components
                .into_iter()
                .map(|(name, data)| (name, data.into_vec()))
                .collect(),
        };
        (self.entity_id, output.into_message())
    }
}
