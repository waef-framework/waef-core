use crate::core::WaefError;
use crate::ecs::component_query::ComponentQuery;
use crate::ecs::{EcsRegistry, IsComponent};
use std::sync::Arc;

use super::component_set::ComponentSet;

pub trait EcsExtension {
    fn define_component<TComponent: IsComponent>(&self) -> Result<(), WaefError>;
    fn attach_component<TComponent: IsComponent>(
        &self,
        eid: u128,
        component: TComponent,
    ) -> Result<(), WaefError>;
    fn detach_component<TComponent: IsComponent>(&self, eid: u128) -> Result<(), WaefError>;

    fn create_entity_with(&self, eid: u128, component: impl ComponentSet) -> Result<(), WaefError>;

    fn apply_reduce<TSeed>(
        &self,
        component_names: &[String],
        seed: TSeed,
        callback: impl Fn(TSeed, u128, &[&mut [u8]]) -> TSeed,
    ) -> TSeed;
    fn apply_map<TResult>(
        &self,
        component_names: &[String],
        callback: impl Fn(u128, &[&mut [u8]]) -> TResult,
    ) -> Vec<TResult>;
    fn apply_choose<TResult>(
        &self,
        component_names: &[String],
        callback: impl Fn(u128, &[&mut [u8]]) -> Option<TResult>,
    ) -> Vec<TResult>;

    fn query<TComponentList: ComponentQuery, TFunction: FnMut(u128, TComponentList)>(
        &self,
        function: TFunction,
    );
    fn query_reduce<
        TSeed,
        TComponentList: ComponentQuery,
        TFunction: FnMut(TSeed, u128, TComponentList) -> TSeed,
    >(
        &self,
        seed: TSeed,
        callback: TFunction,
    ) -> TSeed;
    fn query_map<
        TResult,
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList) -> TResult,
    >(
        &self,
        callback: TFunction,
    ) -> Vec<TResult>;
    fn query_choose<
        TResult,
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList) -> Option<TResult>,
    >(
        &self,
        callback: TFunction,
    ) -> Vec<TResult>;

    fn query_single<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> TResult,
    >(
        &self,
        entity_id: u128,
        function: TFunction,
    ) -> Option<TResult>;

    fn query_set<TComponentList: ComponentQuery, TFunction: FnMut(u128, TComponentList)>(
        &self,
        entity_ids: &[u128],
        function: TFunction,
    );
    fn query_set_reduce<
        TComponentList: ComponentQuery,
        TSeed,
        TFunction: FnMut(TSeed, u128, TComponentList) -> TSeed,
    >(
        &self,
        seed: TSeed,
        entity_ids: &[u128],
        function: TFunction,
    ) -> TSeed;
    fn query_set_map<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> TResult,
    >(
        &self,
        entity_ids: &[u128],
        function: TFunction,
    ) -> Vec<TResult>;
    fn query_set_choose<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> Option<TResult>,
    >(
        &self,
        entity_ids: &[u128],
        function: TFunction,
    ) -> Vec<TResult>;

    fn query_find<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> Option<TResult>,
    >(
        &self,
        function: TFunction,
    ) -> Option<TResult>;
}

impl EcsExtension for Arc<dyn EcsRegistry> {
    fn define_component<TComponent: IsComponent>(&self) -> Result<(), WaefError> {
        self.define_component_by_size(
            TComponent::component_name().into(),
            TComponent::component_size(),
        )
    }

    fn create_entity_with(&self, eid: u128, component: impl ComponentSet) -> Result<(), WaefError> {
        self.create_entity_with_components(eid, component.to_hash_map())
    }

    fn attach_component<TComponent: IsComponent>(
        &self,
        eid: u128,
        component: TComponent,
    ) -> Result<(), WaefError> {
        self.attach_component_buffer(
            TComponent::component_name().into(),
            eid,
            component.into_boxed_slice(),
        )
    }

    fn detach_component<TComponent: IsComponent>(&self, eid: u128) -> Result<(), WaefError> {
        self.detach_component_by_name(TComponent::component_name().into(), eid)
    }
    fn apply_reduce<TSeed>(
        &self,
        component_names: &[String],
        seed: TSeed,
        callback: impl Fn(TSeed, u128, &[&mut [u8]]) -> TSeed,
    ) -> TSeed {
        let mut next = Some(seed);
        self.apply(component_names, &mut |id, data| {
            let value = callback(next.take().unwrap(), id, data);
            _ = next.insert(value);
        });
        next.unwrap()
    }

    fn apply_map<TResult>(
        &self,
        component_names: &[String],
        callback: impl Fn(u128, &[&mut [u8]]) -> TResult,
    ) -> Vec<TResult> {
        let mut result = Vec::with_capacity(32);
        self.apply(component_names, &mut |id, data| {
            result.push(callback(id, data));
        });
        result
    }

    fn apply_choose<TResult>(
        &self,
        component_names: &[String],
        callback: impl Fn(u128, &[&mut [u8]]) -> Option<TResult>,
    ) -> Vec<TResult> {
        let mut result = Vec::with_capacity(32);
        self.apply(component_names, &mut |id, data| {
            if let Some(item) = callback(id, data) {
                result.push(item);
            }
        });
        result
    }

    fn query<TComponentList, TFunction>(&self, mut function: TFunction)
    where
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList),
    {
        let component_names = TComponentList::get_component_names();

        self.apply_unsafe(&component_names[..], &mut |id, data| {
            function(id, TComponentList::from_ptr(data))
        });
    }

    fn query_find<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> Option<TResult>,
    >(
        &self,
        mut function: TFunction,
    ) -> Option<TResult> {
        let component_names = TComponentList::get_component_names();

        let mut result = None;
        self.apply_until_unsafe(&component_names[..], &mut |id, data| {
            result = function(id, TComponentList::from_ptr(data));
            result.is_some()
        });
        result
    }

    fn query_single<TComponentList, TResult, TFunction>(
        &self,
        entity_id: u128,
        mut function: TFunction,
    ) -> Option<TResult>
    where
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList) -> TResult,
    {
        let component_names = TComponentList::get_component_names();

        let mut result = None;
        self.apply_single_unsafe(entity_id, &component_names[..], &mut |id, data| {
            result = Some(function(id, TComponentList::from_ptr(data)));
        });
        result
    }

    fn query_set<TComponentList, TFunction>(&self, entity_ids: &[u128], mut function: TFunction)
    where
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList),
    {
        let component_names = TComponentList::get_component_names();

        self.apply_set_unsafe(entity_ids, &component_names[..], &mut |id, data| {
            function(id, TComponentList::from_ptr(data))
        });
    }

    fn query_reduce<TSeed, TComponentList, TFunction>(
        &self,
        seed: TSeed,
        mut callback: TFunction,
    ) -> TSeed
    where
        TComponentList: ComponentQuery,
        TFunction: FnMut(TSeed, u128, TComponentList) -> TSeed,
    {
        let mut next = Some(seed);
        self.query(|id, components: TComponentList| {
            let value = callback(next.take().unwrap(), id, components);
            _ = next.insert(value);
        });
        next.unwrap()
    }

    fn query_map<TResult, TComponentList, TFunction>(&self, mut callback: TFunction) -> Vec<TResult>
    where
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList) -> TResult,
    {
        let mut result = Vec::with_capacity(32);
        self.query(&mut |id, data: TComponentList| {
            result.push(callback(id, data));
        });
        result
    }

    fn query_choose<TResult, TComponentList, TFunction>(
        &self,
        mut callback: TFunction,
    ) -> Vec<TResult>
    where
        TComponentList: ComponentQuery,
        TFunction: FnMut(u128, TComponentList) -> Option<TResult>,
    {
        let mut result = Vec::with_capacity(32);
        self.query(&mut |id, data: TComponentList| {
            if let Some(item) = callback(id, data) {
                result.push(item);
            }
        });
        result
    }

    fn query_set_reduce<
        TComponentList: ComponentQuery,
        TSeed,
        TFunction: FnMut(TSeed, u128, TComponentList) -> TSeed,
    >(
        &self,
        seed: TSeed,
        entity_ids: &[u128],
        mut callback: TFunction,
    ) -> TSeed {
        let mut next = Some(seed);
        self.query_set(entity_ids, |id, components: TComponentList| {
            let value = callback(next.take().unwrap(), id, components);
            _ = next.insert(value);
        });
        next.unwrap()
    }

    fn query_set_map<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> TResult,
    >(
        &self,
        entity_ids: &[u128],
        mut callback: TFunction,
    ) -> Vec<TResult> {
        let mut result = Vec::with_capacity(32);
        self.query_set(entity_ids, &mut |id, data: TComponentList| {
            result.push(callback(id, data));
        });
        result
    }

    fn query_set_choose<
        TComponentList: ComponentQuery,
        TResult,
        TFunction: FnMut(u128, TComponentList) -> Option<TResult>,
    >(
        &self,
        entity_ids: &[u128],
        mut callback: TFunction,
    ) -> Vec<TResult> {
        let mut result = Vec::with_capacity(32);
        self.query_set(entity_ids, &mut |id, data: TComponentList| {
            if let Some(item) = callback(id, data) {
                result.push(item);
            }
        });
        result
    }
}
