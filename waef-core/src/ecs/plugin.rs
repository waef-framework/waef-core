use super::commands::{
    AttachComponent, CreateEntity, CreateEntityWithComponents, DestroyEntity, DetachComponent,
};
use crate::actors::{ActorAlignment, HasActorAlignment, IsActor};
use crate::actors::{ActorsPlugin, UseActors};
use crate::core::{
    DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, IsMessage, Message,
};
use crate::ecs::component_query::ComponentQuery;
use crate::ecs::extensions::EcsExtension;
use crate::ecs::EcsRegistry;
use crate::timers::messages::OnAppTick;
use std::collections::HashSet;
use std::sync::mpsc::Sender;
use std::sync::Arc;

pub trait EcsOnMessageResult {
    fn send(self, sender: &Sender<Message>);
}
impl EcsOnMessageResult for () {
    fn send(self, _: &Sender<Message>) {}
}
impl EcsOnMessageResult for Message {
    fn send(self, sender: &Sender<Message>) {
        _ = sender.send(self);
    }
}
impl<TImpl: EcsOnMessageResult> EcsOnMessageResult for Option<TImpl> {
    fn send(self, sender: &Sender<Message>) {
        if let Some(message) = self {
            message.send(sender);
        }
    }
}
impl<TImpl: EcsOnMessageResult> EcsOnMessageResult for Vec<TImpl> {
    fn send(self, sender: &Sender<Message>) {
        for item in self.into_iter() {
            item.send(sender);
        }
    }
}

struct EcsSystemOnMessage {
    name: String,
    weight: u32,
    alignment: ActorAlignment,
    message_category: String,
    ecs: Arc<dyn EcsRegistry>,
    sender: Sender<Message>,
    callback: Box<dyn Fn(&Message, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send>,
}

impl IsDispatcher for EcsSystemOnMessage {
    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(HashSet::from([self.message_category.clone()]))
    }
    fn dispatch(&mut self, msg: &Message) -> ExecutionResult {
        (self.callback)(msg, &self.ecs, &self.sender);
        ExecutionResult::Processed
    }
}

impl IsDisposable for EcsSystemOnMessage {
    fn dispose(&mut self) {}
}

impl HasActorAlignment for EcsSystemOnMessage {
    fn alignment(&self) -> ActorAlignment {
        self.alignment
    }
}

impl IsActor for EcsSystemOnMessage {
    fn weight(&self) -> u32 {
        self.weight
    }

    fn name(&self) -> String {
        format!("ecs_{}", self.name)
    }
}

pub struct UseEcsSystems {
    on_message: Vec<(
        String,
        u32,
        String,
        ActorAlignment,
        Box<dyn Fn(&Message, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send>,
    )>,
    ecs: Arc<dyn EcsRegistry>,
    sender: Sender<Message>,
}

impl UseEcsSystems {
    pub fn new(ecs: Arc<dyn EcsRegistry>, sender: Sender<Message>) -> Self {
        Self {
            on_message: Vec::new(),
            ecs,
            sender,
        }
    }

    pub fn per_tick<TComponentList: ComponentQuery, TResult: EcsOnMessageResult>(
        mut self,
        callback: impl Fn(&OnAppTick, u128, TComponentList) -> TResult + Send + 'static,
    ) -> Self {
        let actor_name = format!(
            "{}-{}",
            OnAppTick::category_name(),
            TComponentList::get_component_names().join("-")
        );
        self.on_message.push((
            actor_name,
            32,
            OnAppTick::category_name().into(),
            ActorAlignment::Priority,
            Box::new(
                move |message: &Message, ecs: &Arc<dyn EcsRegistry>, sender: &Sender<Message>| {
                    if let Some(message) = OnAppTick::from_message(&message) {
                        let mut outputs = Vec::with_capacity(32);
                        ecs.query(|id, components: TComponentList| {
                            outputs.push(callback(&message, id, components));
                        });
                        outputs.into_iter().for_each(|x| x.send(sender));
                    }
                },
            ),
        ));
        self
    }

    pub fn per_tick_callback(
        mut self,
        name: impl Into<String>,
        callback: impl Fn(OnAppTick, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send + 'static,
    ) -> Self {
        self.on_message.push((
            name.into(),
            32,
            OnAppTick::category_name().into(),
            ActorAlignment::Priority,
            Box::new(
                move |message: &Message, ecs: &Arc<dyn EcsRegistry>, sender: &Sender<Message>| {
                    if let Some(message) = OnAppTick::from_message(&message) {
                        (callback)(message, ecs, sender);
                    }
                },
            ),
        ));
        self
    }

    pub fn on_message<
        TMessage: IsMessage + 'static,
        TComponentList: ComponentQuery,
        TResult: EcsOnMessageResult,
    >(
        mut self,
        callback: impl Fn(&TMessage, u128, TComponentList) -> TResult + Send + 'static,
    ) -> Self {
        let actor_name = format!(
            "{}-{}",
            TMessage::category_name(),
            TComponentList::get_component_names().join("-")
        );
        self.on_message.push((
            actor_name,
            4,
            TMessage::category_name().into(),
            ActorAlignment::Processing,
            Box::new(
                move |message: &Message, ecs: &Arc<dyn EcsRegistry>, sender: &Sender<Message>| {
                    if let Some(message) = TMessage::from_message(&message) {
                        let mut outputs = Vec::with_capacity(32);
                        ecs.query(|id, components: TComponentList| {
                            outputs.push(callback(&message, id, components))
                        });
                        outputs.into_iter().for_each(|x| x.send(sender))
                    }
                },
            ),
        ));
        self
    }

    pub fn on_message_sub<
        TMessage: IsMessage + 'static,
        TComponentList: ComponentQuery,
        TResult: EcsOnMessageResult,
    >(
        mut self,
        child_filter: impl Into<String>,
        callback: impl Fn(&TMessage, u128, TComponentList) -> TResult + Send + 'static,
    ) -> Self {
        let child_filter = child_filter.into();
        let actor_name = format!(
            "{}:{}-{}",
            TMessage::category_name(),
            child_filter,
            TComponentList::get_component_names().join("-")
        );
        self.on_message.push((
            actor_name,
            4,
            format!("{}:{}", TMessage::category_name(), child_filter),
            ActorAlignment::Processing,
            Box::new(
                move |message: &Message, ecs: &Arc<dyn EcsRegistry>, sender: &Sender<Message>| {
                    if let Some(message) = TMessage::from_message(message) {
                        let mut outputs = Vec::with_capacity(32);
                        ecs.query(|id, components: TComponentList| {
                            outputs.push(callback(&message, id, components));
                        });
                        outputs.into_iter().for_each(|x| x.send(sender));
                    }
                },
            ),
        ));
        self
    }

    pub fn on_message_callback<TMessage: IsMessage + 'static>(
        self,
        name: impl Into<String>,
        callback: impl Fn(TMessage, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send + 'static,
    ) -> Self {
        self.on_message_callback_raw_boxed(
            name,
            TMessage::category_name(),
            Box::new(
                move |message: &Message, ecs: &Arc<dyn EcsRegistry>, sender: &Sender<Message>| {
                    if let Some(message) = TMessage::from_message(message) {
                        (callback)(message, ecs, sender)
                    }
                },
            ),
        )
    }

    pub fn on_message_callback_sub<TMessage: IsMessage + 'static>(
        self,
        name: impl Into<String>,
        category_name: impl Into<String>,
        callback: impl Fn(TMessage, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send + 'static,
    ) -> Self {
        self.on_message_callback_raw_boxed(
            name,
            format!("{}:{}", TMessage::category_name(), category_name.into()),
            Box::new(
                move |message: &Message, ecs: &Arc<dyn EcsRegistry>, sender: &Sender<Message>| {
                    if let Some(message) = TMessage::from_message(message) {
                        (callback)(message, ecs, sender)
                    }
                },
            ),
        )
    }

    pub fn on_message_callback_raw(
        self,
        name: impl Into<String>,
        message_name: impl Into<String>,
        callback: impl Fn(&Message, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send + 'static,
    ) -> Self {
        self.on_message_callback_raw_boxed(name, message_name, Box::new(callback))
    }

    pub fn on_message_callback_raw_boxed(
        mut self,
        name: impl Into<String>,
        message_name: impl Into<String>,
        callback: Box<dyn Fn(&Message, &Arc<dyn EcsRegistry>, &Sender<Message>) + Send>,
    ) -> Self {
        self.on_message.push((
            name.into(),
            4,
            message_name.into(),
            ActorAlignment::Processing,
            callback,
        ));
        self
    }
}

pub trait EcsSystemPlugin {
    fn apply(self, target: UseEcsSystems) -> UseEcsSystems;
}
impl UseEcsSystems {
    pub fn use_plugin<T: EcsSystemPlugin>(self, actor: T) -> Self {
        actor.apply(self)
    }
}

struct EcsActor {
    ecs: Arc<dyn EcsRegistry>,
}

impl IsDispatcher for EcsActor {
    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(
            [
                AttachComponent::category_name().into(),
                DetachComponent::category_name().into(),
                CreateEntity::category_name().into(),
                CreateEntityWithComponents::category_name().into(),
                DestroyEntity::category_name().into(),
            ]
            .into(),
        )
    }

    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        match message.name.as_str() {
            "ecs:attach_component" => {
                if let Some(msg) = AttachComponent::from_message(message) {
                    _ = self.ecs.attach_component_buffer(
                        msg.component_name,
                        msg.entity_id,
                        msg.component_data,
                    );
                    ExecutionResult::Processed
                } else {
                    ExecutionResult::NoOp
                }
            }
            "ecs:detach_component" => {
                if let Some(msg) = DetachComponent::from_message(message) {
                    _ = self
                        .ecs
                        .detach_component_by_name(msg.component_name, msg.entity_id);
                    ExecutionResult::Processed
                } else {
                    ExecutionResult::NoOp
                }
            }
            "ecs:create_entity" => {
                if let Some(msg) = CreateEntity::from_message(message) {
                    _ = self.ecs.create_entity(msg.entity_id);
                    ExecutionResult::Processed
                } else {
                    ExecutionResult::NoOp
                }
            }
            "ecs:create_entity_with_components" => {
                if let Some(msg) = CreateEntityWithComponents::from_message(message) {
                    _ = self.ecs.create_entity_with_components(
                        msg.entity_id,
                        msg.components
                            .into_iter()
                            .map(|(name, data)| (name, data.into_boxed_slice()))
                            .collect(),
                    );
                    ExecutionResult::Processed
                } else {
                    ExecutionResult::NoOp
                }
            }
            "ecs:destroy_entity" => {
                if let Some(msg) = DestroyEntity::from_message(message) {
                    _ = self.ecs.destroy_entity(msg.entity_id);
                    ExecutionResult::Processed
                } else {
                    ExecutionResult::NoOp
                }
            }
            _ => ExecutionResult::NoOp,
        }
    }
}

impl IsDisposable for EcsActor {
    fn dispose(&mut self) {}
}

impl HasActorAlignment for EcsActor {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}

impl IsActor for EcsActor {
    fn weight(&self) -> u32 {
        4
    }

    fn name(&self) -> String {
        "ecs_commands".to_string()
    }
}

impl ActorsPlugin for UseEcsSystems {
    fn apply<T: UseActors>(self, mut target: T) -> T {
        for (name, weight, message_category, alignment, system) in self.on_message.into_iter() {
            target = target.use_actor(Box::new(EcsSystemOnMessage {
                name,
                weight,
                message_category,
                alignment,
                sender: self.sender.clone(),
                ecs: self.ecs.clone(),
                callback: system,
            }));
        }
        target
    }
}

pub struct UseEcsEvents {
    ecs: Arc<dyn EcsRegistry>,
}
impl UseEcsEvents {
    pub fn new(ecs: Arc<dyn EcsRegistry>) -> Self {
        Self { ecs }
    }
}
impl ActorsPlugin for UseEcsEvents {
    fn apply<T: UseActors>(self, target: T) -> T {
        target.use_actor(Box::new(EcsActor {
            ecs: self.ecs.clone(),
        }))
    }
}
