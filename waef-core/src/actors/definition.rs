use std::cmp::max;

use crate::core::CompiledDispatchFilter;

use super::{
    super::core::{DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, Message},
    ActorAlignment, HasActorAlignment, IsActor,
};

pub type ActorOnMessageCallback<T, C> = (C, fn(&mut T, &C, &Message) -> ExecutionResult);
pub type ActorDisposeCallback<T, C> = (C, fn(&mut T, &C));

pub struct ActorDefinition<T, C> {
    name: String,
    weight: u32,
    alignment: ActorAlignment,
    message_handlers: Vec<(DispatchFilter, ActorOnMessageCallback<T, C>)>,
    dispose: Vec<ActorDisposeCallback<T, C>>,
}
impl<T, C> ActorDefinition<T, C> {
    pub fn new(name: impl Into<String>) -> Self {
        ActorDefinition {
            name: name.into(),
            weight: 0,
            alignment: ActorAlignment::Processing,
            message_handlers: Vec::new(),
            dispose: Vec::new(),
        }
    }

    pub fn with_weight(name: impl Into<String>, weight: u32) -> Self {
        ActorDefinition {
            name: name.into(),
            weight,
            alignment: ActorAlignment::Processing,
            message_handlers: Vec::new(),
            dispose: Vec::new(),
        }
    }

    pub fn with_alignment(name: impl Into<String>, alignment: ActorAlignment) -> Self {
        ActorDefinition {
            name: name.into(),
            weight: 0,
            alignment,
            message_handlers: Vec::new(),
            dispose: Vec::new(),
        }
    }

    pub fn with_weight_and_alignment(
        name: impl Into<String>,
        weight: u32,
        alignment: ActorAlignment,
    ) -> Self {
        ActorDefinition {
            name: name.into(),
            weight,
            alignment,
            message_handlers: Vec::new(),
            dispose: Vec::new(),
        }
    }

    pub fn get_name(&self) -> &str {
        self.name.as_str()
    }

    pub fn weight(&mut self, weight: u32) {
        self.weight = weight;
    }

    pub fn alignment(&mut self, alignment: ActorAlignment) {
        self.alignment = alignment
    }

    pub fn on_message(
        &mut self,
        filter: impl Into<DispatchFilter>,
        context: C,
        callback: fn(&mut T, &C, &Message) -> ExecutionResult,
    ) {
        self.message_handlers
            .push((filter.into(), (context, callback)));
    }

    pub fn on_dispose(&mut self, context: C, callback: fn(&mut T, &C) -> ()) {
        self.dispose.push((context, callback));
    }
}

pub struct Actor<T, C> {
    name: String,
    weight: u32,
    data: T,
    alignment: ActorAlignment,
    message_handlers: Vec<(CompiledDispatchFilter, ActorOnMessageCallback<T, C>)>,
    dispose: Vec<ActorDisposeCallback<T, C>>,
    filter: DispatchFilter,
}
impl<T, C> IsDispatcher for Actor<T, C> {
    fn filter(&self) -> DispatchFilter {
        self.filter.clone()
    }

    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        self.message_handlers.iter_mut().fold(
            ExecutionResult::NoOp,
            |result, (filter, (context, callback))| {
                if filter.allows(message) {
                    max(callback(&mut self.data, context, message), result)
                } else {
                    result
                }
            },
        )
    }
}
impl<T, C> IsDisposable for Actor<T, C> {
    fn dispose(&mut self) {
        for (context, callback) in self.dispose.iter_mut() {
            callback(&mut self.data, context)
        }
    }
}
impl<T, C> HasActorAlignment for Actor<T, C> {
    fn alignment(&self) -> ActorAlignment {
        self.alignment
    }
}
impl<T: Send, C: Send> IsActor for Actor<T, C> {
    fn weight(&self) -> u32 {
        self.weight
    }

    fn name(&self) -> String {
        self.name.clone()
    }
}

impl<T, C> ActorDefinition<T, C> {
    pub fn build(self, data: T) -> Actor<T, C> {
        let filter = self
            .message_handlers
            .iter()
            .map(|(filter, _)| filter.clone())
            .fold(DispatchFilter::None, DispatchFilter::merge);

        Actor {
            name: self.name,
            weight: self.weight,
            filter,
            data,
            alignment: self.alignment,
            dispose: self.dispose,
            message_handlers: self
                .message_handlers
                .into_iter()
                .map(|(filter, plugin)| (filter.compile(), plugin))
                .collect(),
        }
    }
}
