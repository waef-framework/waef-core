use super::{ActorAlignment, HasActorAlignment, IsActor};
use crate::core::{CompiledDispatchFilter, DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, Message};
use std::cmp::max;

pub struct ActorCollectionBuilder {
    weight: u32,
    alignment: ActorAlignment,
    actors: Vec<Box<dyn IsActor>>,
}
impl Default for ActorCollectionBuilder {
    fn default() -> Self {
        Self::new()
    }
}
impl ActorCollectionBuilder {
    pub fn new_vec(alignment: ActorAlignment, count: usize) -> Vec<Self> {
        let mut result = Vec::with_capacity(count);
        for _ in 0..count {
            result.push(Self {
                weight: 0,
                alignment,
                actors: Vec::new(),
            });
        }
        result
    }

    pub fn new() -> Self {
        Self {
            weight: 0,
            alignment: ActorAlignment::Processing,
            actors: Vec::new(),
        }
    }

    pub fn weight(&self) -> u32 {
        self.weight
    }

    pub fn alignment(&self) -> ActorAlignment {
        self.alignment
    }

    pub fn add_weight(&mut self, weight: u32) {
        self.weight += weight;
    }

    pub fn set_alignment(&mut self, alignment: ActorAlignment) {
        self.alignment = alignment
    }

    pub fn add_box_mut(&mut self, actor: Box<dyn IsActor>) {
        self.weight += actor.weight();
        self.actors.push(actor);
    }

    pub fn is_empty(&self) -> bool {
        self.actors.is_empty()
    }
}

pub struct ActorCollection {
    name: String,
    filter: DispatchFilter,
    weight: u32,
    alignment: ActorAlignment,
    actors: Vec<(CompiledDispatchFilter, Box<dyn IsActor>)>,
}
impl ActorCollection {
    pub fn new(actors: Vec<Box<dyn IsActor>>) -> Self {
        Self {
            filter: actors.iter().fold(DispatchFilter::None, |filter, actor| {
                DispatchFilter::merge(filter, actor.filter())
            }),
            name: actors
                .iter()
                .map(|actor| actor.name())
                .collect::<Vec<_>>()
                .join(":"),
            alignment: actors
                .iter()
                .fold(ActorAlignment::Processing, |prev, actor| {
                    match (prev, actor.alignment()) {
                         (ActorAlignment::IO, _) | (_, ActorAlignment::IO) => ActorAlignment::IO,
                        (ActorAlignment::Priority, _) | (_, ActorAlignment::Priority) => {
                            ActorAlignment::Priority
                        }
                        _ => ActorAlignment::Processing,
                    }
                }),
            weight: actors
                .iter()
                .fold(0, |weight, actor| weight + actor.weight()),
            actors: actors.into_iter().map(|actor| (actor.filter().compile(), actor)).collect(),
        }
    }

    pub fn len(&self) -> usize {
        self.actors.len()
    }

    pub fn actors(&self) -> impl Iterator<Item = &Box<dyn IsActor>> {
        self.actors.iter().map(|f| &f.1)
    }

    pub fn into_actors(self) -> Vec<Box<dyn IsActor>> {
        self.actors.into_iter().map(|f| f.1).collect()
    }
}
impl HasActorAlignment for ActorCollection {
    fn alignment(&self) -> ActorAlignment {
        self.alignment
    }
}
impl IsDispatcher for ActorCollection {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        let _guard = tracing::trace_span!(
            "actor_collection::dispatch",
            actors = self.name(),
            message_name = message.name,
            message_len = message.buffer.len()
        )
        .entered();

        let mut result = ExecutionResult::NoOp;
        for (filter, actor) in self.actors.iter_mut() {
            if filter.allows(message) {
                let _guard = tracing::trace_span!(
                    "actor_collection::dispatch::to_actor",
                    actor = actor.name(),
                    message_name = message.name,
                    message_len = message.buffer.len()
                )
                .entered();

                result = max(result, actor.dispatch(message));
            }
        }
        result
    }

    fn filter(&self) -> DispatchFilter {
        self.filter.clone()
    }
}
impl IsDisposable for ActorCollection {
    fn dispose(&mut self) {
        let _guard =
            tracing::trace_span!("actor_collection::dispose", actors = self.name()).entered();

        for (_, actor) in self.actors.iter_mut() {
            actor.dispose()
        }
    }
}
impl IsActor for ActorCollection {
    fn weight(&self) -> u32 {
        self.weight
    }

    fn name(&self) -> String {
        self.name.clone()
    }
}
impl ActorCollectionBuilder {
    pub fn build(self) -> ActorCollection {
        ActorCollection {
            name: self
                .actors
                .iter()
                .map(|a| a.name())
                .reduce(|lhs, rhs| lhs + ":" + rhs.as_str())
                .unwrap_or("".to_string()),
            filter: self
                .actors
                .iter()
                .map(|a| a.filter())
                .fold(DispatchFilter::None, DispatchFilter::merge),
            weight: self.weight,
            alignment: self.alignment,
            actors: self.actors.into_iter().map(|f| (f.filter().compile(), f)).collect(),
        }
    }
}
