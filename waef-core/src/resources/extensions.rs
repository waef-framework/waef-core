use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    sync::Arc,
};

use crate::util::pod::Pod;

use super::ResourceStore;

pub trait ResourcesExtensions {
    fn remove_pod<T: Pod>(&self, key: u128) -> Option<T>;
    fn put_pod<T: Pod>(&self, key: u128, data: &T);
    fn get_pod<T: Pod>(&self, key: u128) -> Option<T>;

    fn remove_singleton<T: Pod>(&self) -> Option<T>;
    fn put_singleton<T: Pod>(&self, data: &T);
    fn get_singleton<T: Pod>(&self) -> Option<T>;

    fn get_string(&self, key: u128) -> Option<String>;
    fn put_string(&self, key: u128, value: String);
}

fn hash_type_id<T: Pod>() -> u128 {
    let mut hasher = DefaultHasher::new();
    std::any::TypeId::of::<T>().hash(&mut hasher);
    u128::from_bytes([0, hasher.finish()].try_split())
}

impl ResourcesExtensions for Arc<dyn ResourceStore> {
    fn get_string(&self, key: u128) -> Option<String> {
        self.get_cloned(key)
            .and_then(|buffer| String::from_utf8(buffer).ok())
    }

    fn put_string(&self, key: u128, value: String) {
        self.put(key, value.as_bytes())
    }

    fn put_pod<T: Pod>(&self, key: u128, data: &T) {
        self.put(key, data.as_bytes())
    }

    fn get_pod<T: Pod>(&self, key: u128) -> Option<T> {
        let mut buffer = T::zeroed();
        if let Some(read_bytes) = self.get(key, buffer.as_bytes_mut()) {
            if read_bytes == T::byte_count() {
                Some(buffer)
            } else {
                None
            }
        } else {
            None
        }
    }

    fn put_singleton<T: Pod>(&self, data: &T) {
        self.put_pod(hash_type_id::<T>(), data)
    }

    fn get_singleton<T: Pod>(&self) -> Option<T> {
        self.get_pod(hash_type_id::<T>())
    }

    fn remove_pod<T: Pod>(&self, key: u128) -> Option<T> {
        self.remove(key).map(|data| Pod::from_bytes(&data))
    }

    fn remove_singleton<T: Pod>(&self) -> Option<T> {
        self.remove_pod::<T>(hash_type_id::<T>())
    }
}
