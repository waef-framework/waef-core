pub mod collection;
pub mod definition;

use serde::{Deserialize, Serialize};

use crate::core::{IsDispatcher, IsDisposable};

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Eq, Hash)]
pub enum ActorAlignment {
    Processing,
    IO,
    Priority,
}

pub trait HasActorAlignment {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}

pub trait IsActor: IsDispatcher + IsDisposable + HasActorAlignment + Send {
    fn weight(&self) -> u32 {
        0
    }

    fn name(&self) -> String {
        std::any::type_name::<Self>().into()
    }
}

pub trait UseActors {
    fn use_actor(self, actor: Box<dyn IsActor>) -> Self;
}

pub trait ActorsPlugin {
    fn apply<T: UseActors>(self, target: T) -> T;
}

pub trait ActorsInjector {
    fn inject<T: UseActors>(&self, target: T) -> T;
}

#[macro_export]
macro_rules! if_some_message_return {
    ($message:expr, ($param:ident : $struct:ty) $(where $filter:expr)? => $handler:expr) => {
        if let Some($param) = $crate::Message::as_message::<$struct>($message){
            $(if $filter)?  {
                return $handler;
            }
        }
    };
    ($message:expr, (_ : $struct:ty) $(where $filter:expr)? => $handler:expr) => {
        if let Some(_) = $crate::Message::as_message::<$struct>($message){
            $(if $filter)?  {
                return $handler;
            }
        }
    };
    ($message:expr, ($param:ident : $struct:ty) + $extension:tt $(where $filter:expr)? => $handler:expr) => {
        if let Some($param) = $crate::Message::as_message_with::<$struct>($message, $extension){
            $(if $filter)?  {
                return $handler;
            }
        }
    };
    ($message:expr, (_ : $struct:ty) + $extension:tt $(where $filter:expr)? => $handler:expr) => {
        if let Some(_) = $crate::Message::as_message_with::<$struct>($message, $extension){
            $(if $filter)?  {
                return $handler;
            }
        }
    };
}

#[macro_export]
macro_rules! match_message {
    ($message:expr, { $(($param:tt : $struct:ty) $(+ $extension:tt)? $(where $filter:expr)? => $handler:expr),*, _ => $fallback:expr }) => {
        (||{
            $($crate::if_some_message_return!($message, ($param: $struct) $(+ $extension)? $(where $filter)? => $handler);)*
            $fallback
        })()
    };
}

#[macro_export]
macro_rules! message_handler_to_filter_name {
    (($param:tt : $struct:ty) => $handler:expr) => {
        <$struct as $crate::IsMessage>::category_name().into()
    };
    (($param:tt : $struct:ty) + $extension:tt => $handler:expr) => {
        format!("{}:{}", <$struct as $crate::IsMessage>::category_name(), $extension).into()
    };
}

#[macro_export]
macro_rules! match_message_to_filter {
    ({ $(($param:tt : $struct:ty) $(+ $extension:tt)? $(where $filter:expr)? => $handler:expr),*, _ => $fallback:expr }) => {
        $crate::DispatchFilter::OneOf([
            $($crate::message_handler_to_filter_name!(($param: $struct) $(+ $extension)? => $handler),)*
        ].into())
    };
}

#[macro_export(local_inner_macros)]
macro_rules! impl_dispatcher {
    {$($actor:ty => $self:ident $dispatch:tt)+
    } => {
        $(
        impl $crate::IsDispatcher for $actor {
            fn filter(&self) -> $crate::DispatchFilter {
                $crate::match_message_to_filter!($dispatch)
            }
            fn dispatch(&mut $self, message: &$crate::Message) -> ExecutionResult {
                $crate::match_message!(message, $dispatch)
            }
        }
    )+};
}

#[macro_export]
macro_rules! impl_actor {
    {$($actor:ty => $self: ident {
        $(name => $name:expr, )?
        $(alignment => $alignment:expr, )?
        $(weight => $weight:expr, )?
        dispatch => $dispatch:tt
        $(, dispose => $dispose:expr)?
    })+} => {
        $(
        impl $crate::HasActorAlignment for $actor {
            $(fn alignment(&self) -> $crate::ActorAlignment {
                $alignment
            })?
        }

        impl $crate::IsActor for $actor {
            $(fn weight(&self) -> u32 {
                $weight
            })?

            $(fn name(&self) -> String {
                $name.into()
            })?
        }

        impl $crate::IsDisposable for $actor {
            $(fn dispose(&mut $self) {
                $dispose
            })?
        }
        $crate::impl_dispatcher!($actor => $self $dispatch);
    )+};
}

#[cfg(test)]
mod tests {
    use crate::{ExecutionResult, IsDispatcher, IsMessage, Message};

    use super::ActorAlignment;

    struct TestActor {}
    impl TestActor {
        fn some_func(&self) {}
    }
    impl_actor!(TestActor => self {
        name => "test_actor",
        alignment => ActorAlignment::Processing,
        weight => 32,
        dispatch => {
            (msg: TestMessage) + "123" => {
                self.some_func();
                assert_eq!(msg.value, "123");
                ExecutionResult::Processed
            },
            _ => ExecutionResult::NoOp
        },
        dispose => {
            self.some_func()
        }
    });

    struct TestDispatcher {}
    impl TestDispatcher {
        fn some_func(&self) {}
    }
    impl_dispatcher! {
        TestDispatcher => self {
            (_other1: OtherMessage1) => ExecutionResult::NoOp,
            (msg: TestMessage) => {
                self.some_func();
                assert_eq!(msg.value, "123");
                ExecutionResult::Processed
            },
            (_other2: OtherMessage2) => ExecutionResult::NoOp,
            _ => ExecutionResult::NoOp
        }
    }

    struct TestMessage {
        value: String,
    }
    impl IsMessage for TestMessage {
        fn into_message(self) -> Message {
            Message::new_from_bytes_slice(format!("{}:{}:suffix", Self::category_name(), self.value), self.value.as_bytes())
        }

        fn from_message(msg: &Message) -> Option<Self>
        where
            Self: Sized,
        {
            Some(Self {
                value: String::from_utf8_lossy(&msg.buffer).into_owned(),
            })
        }

        fn category_name() -> &'static str {
            "test_message"
        }
    }

    struct OtherMessage1 {}
    impl IsMessage for OtherMessage1 {
        fn into_message(self) -> Message {
            Message::new(Self::category_name())
        }

        fn from_message(_: &Message) -> Option<Self>
        where
            Self: Sized,
        {
            Some(Self {})
        }

        fn category_name() -> &'static str {
            "other_message_1"
        }
    }

    struct OtherMessage2 {}
    impl IsMessage for OtherMessage2 {
        fn into_message(self) -> Message {
            Message::new(Self::category_name())
        }

        fn from_message(_: &Message) -> Option<Self>
        where
            Self: Sized,
        {
            Some(Self {})
        }

        fn category_name() -> &'static str {
            "other_message_2"
        }
    }

    #[test]
    pub fn test_message_match() {
        let test_message = TestMessage {
            value: "123".into(),
        }
        .into_message();

        let matched = match_message!(&test_message, {
            (_other1: OtherMessage1) => false,
            (msg: TestMessage) => {
                assert_eq!(msg.value, "123");
                true
            },
            (_other2: OtherMessage2) => false,
            _ => false
        });
        assert!(matched);
    }

    #[test]
    pub fn test_message_match_where() {
        let test_message = TestMessage {
            value: "123".into(),
        }
        .into_message();

        let matched = match_message!(&test_message, {
            (_: OtherMessage1) => false,
            (msg: TestMessage) where msg.value == "123" => {
                assert_eq!(msg.value, "123");
                true
            },
            (msg: TestMessage) => {
                assert_eq!(msg.value, "124");
                true
            },
            (_other2: OtherMessage2) => false,
            _ => false
        });
        assert!(matched);
    }

    #[test]
    pub fn test_message_match_literal() {
        let test_message = TestMessage {
            value: "123".into(),
        }
        .into_message();

        let matched = match_message!(&test_message, {
            (_: OtherMessage1) => false,
            (msg: TestMessage) + "123" => {
                assert_eq!(msg.value, "123");
                true
            },
            (msg: TestMessage) => {
                assert_eq!(msg.value, "124");
                true
            },
            (_other2: OtherMessage2) => false,
            _ => false
        });
        assert!(matched);
    }
    
    #[test]
    pub fn test_message_match_literal_where() {
        let test_message = TestMessage {
            value: "123".into(),
        }
        .into_message();

        let matched = match_message!(&test_message, {
            (_: OtherMessage1) => false,
            (msg: TestMessage) + "123" where msg.value == "123" => {
                assert_eq!(msg.value, "123");
                true
            },
            (msg: TestMessage) => {
                assert_eq!(msg.value, "124");
                true
            },
            (_other2: OtherMessage2) => false,
            _ => false
        });
        assert!(matched);
    }

    #[test]
    pub fn test_message_match_tokentree_where() {
        let target = "123";
        let test_message = TestMessage {
            value: "123".into(),
        }
        .into_message();

        let matched = match_message!(&test_message, {
            (_: OtherMessage1) => false,
            (msg: TestMessage) + target where msg.value == target => {
                assert_eq!(msg.value, "123");
                true
            },
            (_msg: TestMessage) => {
                false
            },
            (_other2: OtherMessage2) => false,
            _ => false
        });
        assert!(matched);
    }

    #[test]
    pub fn test_dispatcher() {
        let mut actor = TestDispatcher {};
        assert!(actor.filter().allows(&OtherMessage1 {}.into_message()));
        assert!(actor.filter().allows(&OtherMessage2 {}.into_message()));
        assert!(actor
            .filter()
            .allows(&TestMessage { value: "".into() }.into_message()));

        assert_eq!(
            ExecutionResult::Processed,
            actor.dispatch(
                &TestMessage {
                    value: "123".into()
                }
                .into_message()
            )
        );
    }

    #[test]
    pub fn test_actor() {
        let mut actor = TestActor {};
        assert!(!actor.filter().allows(&OtherMessage1 {}.into_message()));
        assert!(!actor.filter().allows(&OtherMessage2 {}.into_message()));
        assert!(!actor
            .filter()
            .allows(&TestMessage { value: "456".into() }.into_message()));
        assert!(actor
            .filter()
            .allows(&TestMessage { value: "123".into() }.into_message()));

        assert_eq!(
            ExecutionResult::Processed,
            actor.dispatch(
                &TestMessage {
                    value: "123".into()
                }
                .into_message()
            )
        );
    }
}
