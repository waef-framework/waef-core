use std::time::Duration;

use crate::core::{IsMessage, Message};
use crate::util::pod::Pod;

#[derive(Copy, Clone)]
pub struct OnAppTick {
    pub delta_time: f64,
}
unsafe impl Pod for OnAppTick {}

impl OnAppTick {
    pub fn new(delta_time: f64) -> Self {
        Self { delta_time }
    }
    
    pub fn from_duration(delta_time: Duration) -> Self {
        Self { delta_time: delta_time.as_secs_f64() }
    }
}

impl IsMessage for OnAppTick {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        OnAppTick::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "app:tick"
    }
}

pub struct OnTimer {
    pub timer_name: String,
    pub delta_time: f64,
}

impl OnTimer {
    pub fn new(timer_name: impl Into<String>, delta_time: f64) -> Self {
        Self { timer_name: timer_name.into(),  delta_time }
    }
    
    pub fn from_duration(timer_name: impl Into<String>, delta_time: Duration) -> Self {
        Self { timer_name: timer_name.into(), delta_time: delta_time.as_secs_f64() }
    }
}

impl IsMessage for OnTimer {
    fn into_message(self) -> Message {
        Message::new_from_pod(format!("{}:{}", Self::category_name(), self.timer_name), self.delta_time)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        f64::try_from_bytes(&msg.buffer).map(|delta_time| Self {
            delta_time,
            timer_name: msg.name[Self::category_name().len() ..].to_string()
        })
    }

    fn category_name() -> &'static str {
        "app:on_timer"
    }
}
