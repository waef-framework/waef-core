use std::{sync::mpsc::Sender, time::Duration};

use crate::core::{IsDisposable, IsMessage, IsTickable, Message};
use crate::DispatchFilter;

use super::messages::OnTimer;
use super::{IsTimer, TimersPlugin, UseTimers};

pub struct SimpleTimer<MessageType: IsMessage> {
    create_message: Box<dyn Fn(Duration) -> MessageType + Send>,
    tick_length: Duration,
    dispatcher: Sender<Message>,
}
impl<MessageType: IsMessage> IsTimer for SimpleTimer<MessageType> {
    fn name(&self) -> String {
        "tick".to_string()
    }
    fn duration(&self) -> Duration {
        self.tick_length
    }

    fn outputs(&self) -> DispatchFilter {
        DispatchFilter::OneOf(["app:tick".into()].into())
    }
}
impl<MessageType: IsMessage> IsTickable for SimpleTimer<MessageType> {
    fn tick(&mut self, delta: Duration) {
        match self
            .dispatcher
            .send((self.create_message)(delta).into_message())
        {
            Ok(_) => (),
            Err(_) => tracing::error!("TickDispatchActor failed to send the system tick"),
        }
    }
}
impl<MessageType: IsMessage> IsDisposable for SimpleTimer<MessageType> {
    fn dispose(&mut self) {}
}

impl<MessageType: IsMessage + 'static> TimersPlugin for SimpleTimer<MessageType> {
    fn apply<T: UseTimers>(self, target: T) -> T {
        target.use_timer(Box::new(self))
    }
}
impl<MessageType: IsMessage> SimpleTimer<MessageType> {
    pub fn new(
        create_message: impl Fn(Duration) -> MessageType + Send + 'static,
        tick_length: Duration,
        dispatcher: Sender<Message>,
    ) -> Self {
        Self {
            create_message: Box::new(create_message),
            tick_length,
            dispatcher,
        }
    }
}

pub struct NamedTimer {
    timer_name: String,
    tick_length: Duration,
    dispatcher: Sender<Message>,
}
impl IsTimer for NamedTimer {
    fn name(&self) -> String {
        format!("timer_{}", self.timer_name)
    }
    fn duration(&self) -> Duration {
        self.tick_length
    }

    fn outputs(&self) -> DispatchFilter {
        DispatchFilter::OneOf(
            [format!("{}:{}", OnTimer::category_name(), self.timer_name).into()].into(),
        )
    }
}
impl IsTickable for NamedTimer {
    fn tick(&mut self, delta: Duration) {
        match self
            .dispatcher
            .send(OnTimer::from_duration(self.timer_name.clone(), delta).into_message())
        {
            Ok(_) => (),
            Err(_) => tracing::error!(
                "NamedTimer::{} failed to send the timer tick",
                self.timer_name
            ),
        }
    }
}
impl IsDisposable for NamedTimer {
    fn dispose(&mut self) {}
}

impl TimersPlugin for NamedTimer {
    fn apply<T: UseTimers>(self, target: T) -> T {
        target.use_timer(Box::new(self))
    }
}
impl NamedTimer {
    pub fn new(
        timer_name: impl Into<String>,
        tick_length: Duration,
        dispatcher: Sender<Message>,
    ) -> Self {
        Self {
            timer_name: timer_name.into(),
            tick_length,
            dispatcher,
        }
    }
}
