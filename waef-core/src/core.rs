use ::serde::{Deserialize, Serialize};

use super::util::pod::Pod;
use std::{
    collections::{HashMap, HashSet}, fmt, ptr::null_mut, sync::Arc, time::Duration
};

#[derive(Debug)]
pub struct WaefError {
    message: String,
}

impl std::error::Error for WaefError {}

impl fmt::Display for WaefError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl WaefError {
    pub fn new(value: impl ToString) -> Self {
        Self {
            message: value.to_string(),
        }
    }

    pub fn merge(lhs: Self, rhs: Self) -> Self {
        WaefError::new(lhs.message + "\n" + rhs.message.as_str())
    }

    pub fn combine(errors: &[Self]) -> Option<Self> {
        if errors.is_empty() {
            None
        } else {
            let msg = errors
                .iter()
                .fold("".to_string(), |data, err| data + err.message.as_str());

            Some(WaefError::new(msg))
        }
    }

    pub fn combine_unchecked(errors: &[Self]) -> Self {
        let msg = errors
            .iter()
            .fold("".to_string(), |data, err| data + err.message.as_str());

        WaefError::new(msg)
    }
}

pub type Result<T> = std::result::Result<T, WaefError>;

#[derive(PartialOrd, PartialEq, Eq, Ord, Debug)]
pub enum ExecutionResult {
    NoOp = 0,
    Processed = 1,
    Kill = 2,
}
impl fmt::Display for ExecutionResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ExecutionResult::NoOp => write!(f, "ExecutionResult::NoOp"),
            ExecutionResult::Processed => write!(f, "ExecutionResult::Processed"),
            ExecutionResult::Kill => write!(f, "ExecutionResult::Kill"),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Message {
    pub name: String,
    pub buffer: Arc<[u8]>,
}
impl Clone for Message {
    fn clone(&self) -> Self {
        Self {
            name: self.name.clone(),
            buffer: self.buffer.clone(),
        }
    }
}

impl std::fmt::Debug for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Message")
            .field("name", &self.name)
            .field("buffer", &self.buffer)
            .finish()
    }
}

impl Message {
    pub fn new(name: impl Into<String>) -> Self {
        Self::new_from_boxed_bytes(name, Box::new([]))
    }

    pub fn new_from_boxed_bytes(name: impl Into<String>, buffer: Box<[u8]>) -> Self {
        Self {
            name: name.into(),
            buffer: buffer.into(),
        }
    }

    pub fn new_from_bytes_slice(name: impl Into<String>, pod: &[u8]) -> Self {
        Message::new_from_boxed_bytes(name.into(), pod.to_owned().into_boxed_slice())
    }

    pub fn new_from_pod<T: Pod>(name: impl Into<String>, pod: T) -> Self {
        Message::new_from_boxed_bytes(name, pod.into_boxed_slice())
    }

    pub fn is_message<TMessage: IsMessage>(msg: &Message) -> bool {
        let target = TMessage::category_name();
        msg.name.starts_with(target)
            && (msg.name.len() == target.len()
                || unsafe { msg.name.chars().nth(target.len()).unwrap_unchecked() } == ':')
    }

    pub fn is_message_with<TMessage: IsMessage>(
        msg: &Message,
        extension: impl Into<String>,
    ) -> bool {
        let prefix = TMessage::category_name();
        let extension = extension.into();
        let full_target_len = prefix.len() + 1 + extension.len();

        if !msg.name.starts_with(prefix) {
            false
        } else {
            if msg.name.len() == full_target_len {
                (unsafe { msg.name.chars().nth(prefix.len()).unwrap_unchecked() == ':' }
                    && msg.name[prefix.len() + 1..] == extension)
            } else {
                msg.name.len() > full_target_len
                    && unsafe { msg.name.chars().nth(prefix.len()).unwrap_unchecked() == ':' }
                    && msg.name[prefix.len() + 1..full_target_len] == extension
                    && unsafe { msg.name.chars().nth(full_target_len).unwrap_unchecked() == ':' }
            }
        }
    }

    pub fn as_message<TMessage: IsMessage>(msg: &Message) -> Option<TMessage> {
        if Self::is_message::<TMessage>(msg) {
            TMessage::from_message(msg)
        } else {
            None
        }
    }

    pub fn as_message_with<TMessage: IsMessage>(
        msg: &Message,
        extension: impl Into<String>,
    ) -> Option<TMessage> {
        if Self::is_message_with::<TMessage>(msg, extension) {
            TMessage::from_message(msg)
        } else {
            None
        }
    }
}

pub trait IsMessage {
    fn into_message(self) -> Message;
    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized;
    fn category_name() -> &'static str;
}

pub mod serde {
    use serde::{de::DeserializeOwned, Serialize};

    use crate::core::{IsMessage, Message, WaefError};

    pub fn serialize_message<T: ?Sized + Serialize + IsMessage>(
        item: &T,
    ) -> Result<Message, WaefError> {
        let buffer = bincode::serialize(&item)
            .map_err(WaefError::new)?
            .into_boxed_slice();

        Ok(Message::new_from_boxed_bytes(T::category_name(), buffer))
    }

    pub fn deserialize_message<T: DeserializeOwned + IsMessage>(
        item: &Message,
    ) -> Result<T, WaefError> {
        bincode::deserialize::<T>(&item.buffer).map_err(WaefError::new)
    }
}

#[derive(Clone)]
enum CompiledDispatchPatternStep {
    Any,
    Part {
        tail: HashMap<String, CompiledDispatchPatternStep>,
    },
}

impl CompiledDispatchPatternStep {
    pub fn allows(&self, message: &str) -> bool {
        match self {
            CompiledDispatchPatternStep::Any => true,
            CompiledDispatchPatternStep::Part { tail } => {
                let mut head = tail;
                for part in message.split(':') {
                    if let Some(next) = head.get(part) {
                        match next {
                            CompiledDispatchPatternStep::Any => return true,
                            CompiledDispatchPatternStep::Part { tail } => {
                                head = tail;
                            }
                        }
                    } else {
                        return false;
                    }
                }
                return false;
            }
        }
    }
}

#[derive(Clone)]
pub struct CompiledDispatchFilter(CompiledDispatchPatternStep);

impl CompiledDispatchFilter {
    pub fn allows(&self, message: &Message) -> bool {
        self.0.allows(&message.name)
    }
}

#[derive(Clone)]
pub enum DispatchFilter {
    None,
    OneOf(HashSet<String>),
    Any,
}
impl Into<DispatchFilter> for String {
    fn into(self) -> DispatchFilter {
        DispatchFilter::OneOf([self].into())
    }
}
impl Into<DispatchFilter> for &str {
    fn into(self) -> DispatchFilter {
        DispatchFilter::OneOf([self.into()].into())
    }
}

impl DispatchFilter {
    pub fn merge(lhs: DispatchFilter, rhs: DispatchFilter) -> DispatchFilter {
        match (lhs, rhs) {
            (DispatchFilter::Any, _) | (_, DispatchFilter::Any) => DispatchFilter::Any,
            (DispatchFilter::None, other) | (other, DispatchFilter::None) => other,
            (DispatchFilter::OneOf(mut lhs), DispatchFilter::OneOf(rhs)) => {
                lhs.extend(rhs.into_iter());
                DispatchFilter::OneOf(lhs)
            }
        }
    }

    pub fn contains_any(&self, other: DispatchFilter) -> bool {
        match self {
            DispatchFilter::None => false,
            DispatchFilter::OneOf(lhs) => match other {
                DispatchFilter::None => false,
                DispatchFilter::OneOf(rhs) => lhs.intersection(&rhs).count() > 0,
                DispatchFilter::Any => !lhs.is_empty(),
            },
            DispatchFilter::Any => match other {
                DispatchFilter::None => false,
                DispatchFilter::OneOf(rhs) => !rhs.is_empty(),
                DispatchFilter::Any => true,
            },
        }
    }

    pub fn contains_all(&self, other: DispatchFilter) -> bool {
        match self {
            DispatchFilter::None => false,
            DispatchFilter::OneOf(lhs) => match other {
                DispatchFilter::None => false,
                DispatchFilter::OneOf(rhs) => lhs.intersection(&rhs).count() == lhs.len(),
                DispatchFilter::Any => !lhs.is_empty(),
            },
            DispatchFilter::Any => match other {
                DispatchFilter::None => false,
                DispatchFilter::OneOf(rhs) => !rhs.is_empty(),
                DispatchFilter::Any => true,
            },
        }
    }

    pub fn allows(&self, msg: &Message) -> bool {
        match self {
            DispatchFilter::None => false,
            DispatchFilter::OneOf(set) => {
                if set.contains("") {
                    return true;
                }
                if set.contains(&msg.name) {
                    return true;
                }

                let parts: Vec<&str> = msg.name.split(':').collect::<Vec<&str>>();
                let mut accum = parts[0].to_string();
                for i in parts.iter().skip(1) {
                    if set.contains(&accum) {
                        return true;
                    }
                    accum += ":";
                    accum += i;
                }
                false
            }
            DispatchFilter::Any => true,
        }
    }

    pub fn compile(self) -> CompiledDispatchFilter {
        CompiledDispatchFilter(unsafe {
            match self {
                DispatchFilter::None => CompiledDispatchPatternStep::Part {
                    tail: HashMap::new(),
                },
                DispatchFilter::OneOf(hash_set) => {
                    let mut chains = CompiledDispatchPatternStep::Part {
                        tail: HashMap::with_capacity(hash_set.len()),
                    };
                    for name in hash_set {
                        let mut head: *mut _ = &mut chains;
                        let parts: Vec<_> = name.split(':').collect();
                        for part in parts {
                            match &mut *head {
                                CompiledDispatchPatternStep::Any => {
                                    head = null_mut();
                                    break
                                },
                                CompiledDispatchPatternStep::Part { tail } => {
                                    if !tail.contains_key(part) {
                                        tail.insert(
                                            part.to_string(),
                                            CompiledDispatchPatternStep::Part {
                                                tail: HashMap::new(),
                                            },
                                        );
                                    }
                                    let next_tail = tail.get_mut(part).unwrap();
                                    match next_tail {
                                        CompiledDispatchPatternStep::Any => {
                                            head = null_mut();
                                            break
                                        },
                                        CompiledDispatchPatternStep::Part { tail: _ } => {
                                            head = next_tail;
                                        }
                                    }
                                }
                            }
                        }
                        if head != null_mut() {
                            *head = CompiledDispatchPatternStep::Any;
                        }
                    }
                    chains
                }
                DispatchFilter::Any => CompiledDispatchPatternStep::Any,
            }
        })
    }
}

pub trait IsDispatcher {
    fn filter(&self) -> DispatchFilter {
        DispatchFilter::Any
    }

    fn dispatch(&mut self, message: &Message) -> ExecutionResult;
}

pub trait IsTickable {
    fn tick(&mut self, delta: Duration);
}

pub trait IsDisposable {
    fn dispose(&mut self) {}
}

#[cfg(test)]
mod tests {
    use super::{DispatchFilter, Message};

    #[quickcheck_macros::quickcheck]
    fn test_dispatch_filter_any_allows_everything(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::Any;
        let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_dispatch_filter_none_allows_nothing(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::None;
        let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
        assert!(!filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_dispatch_filter_one_of_allows_substrings(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::OneOf(
            ["prefix".to_string(), "other:".to_string()]
                .into_iter()
                .collect(),
        );
        let msg =
            Message::new_from_bytes_slice("prefix:".to_string() + &any_string, any_data.as_slice());
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_dispatch_filter_one_of_allows_exact_matches(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::OneOf([any_string.clone()].into_iter().collect());
        let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_parts_allows_sub_parts(
        part_1: String,
        part_2: String,
        part_3: String,
        any_data: Vec<u8>,
    ) {
        let filter =
            DispatchFilter::OneOf([format!("{}:{}", part_1, part_2)].into_iter().collect());
        let msg = Message::new_from_bytes_slice(
            format!("{}:{}:{}", part_1, part_2, part_3),
            any_data.as_slice(),
        );
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_dispatch_filter_one_of_blocks_wrong_prefixes(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::OneOf(["prefix".to_string()].into_iter().collect());
        let msg =
            Message::new_from_bytes_slice("other:".to_string() + &any_string, any_data.as_slice());
        assert!(!filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_dispatch_filter_one_of_blocks_wrong_others(any_string: String, any_data: Vec<u8>) {
        if !any_string.starts_with("prefix:") {
            let filter = DispatchFilter::OneOf(["prefix".to_string()].into_iter().collect());
            let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
            assert!(!filter.allows(&msg));
        }
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_any_allows_everything(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::Any.compile();
        let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_none_allows_nothing(any_string: String, any_data: Vec<u8>) {
        let filter = DispatchFilter::None.compile();
        let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
        assert!(!filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_one_of_allows_substrings(
        any_string: String,
        any_data: Vec<u8>,
    ) {
        let filter = DispatchFilter::OneOf(
            ["prefix".to_string(), "other:".to_string()]
                .into_iter()
                .collect(),
        )
        .compile();
        let msg =
            Message::new_from_bytes_slice("prefix:".to_string() + &any_string, any_data.as_slice());
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_one_of_allows_exact_matches(
        any_string: String,
        any_data: Vec<u8>,
    ) {
        let filter = DispatchFilter::OneOf([any_string.clone()].into_iter().collect()).compile();
        let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
        assert!(filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_one_of_blocks_wrong_prefixes(
        any_string: String,
        any_data: Vec<u8>,
    ) {
        let filter = DispatchFilter::OneOf(["prefix".to_string()].into_iter().collect()).compile();
        let msg =
            Message::new_from_bytes_slice("other:".to_string() + &any_string, any_data.as_slice());
        assert!(!filter.allows(&msg));
    }

    #[quickcheck_macros::quickcheck]
    fn test_compileddispatch_filter_one_of_blocks_wrong_others(
        any_string: String,
        any_data: Vec<u8>,
    ) {
        if !any_string.starts_with("prefix:") {
            let filter =
                DispatchFilter::OneOf(["prefix".to_string()].into_iter().collect()).compile();
            let msg = Message::new_from_bytes_slice(any_string, any_data.as_slice());
            assert!(!filter.allows(&msg));
        }
    }

    #[test]
    fn test_compileddispatch_filter_merged_result() {
        let filter = DispatchFilter::merge(
            DispatchFilter::merge(
                DispatchFilter::None,
                DispatchFilter::OneOf(["wgpu".into()].into()),
            ),
            DispatchFilter::OneOf(
                [
                    "wgpu:subset:message".into(),
                    "other:subset:message".into(),
                    "another:subset:message".into(),
                ]
                .into(),
            ),
        )
        .compile();

        assert!(!filter.allows(&Message::new("physics2d:some:message")));
        assert!(filter.allows(&Message::new("wgpu")));
        assert!(filter.allows(&Message::new("wgpu:any")));
        assert!(filter.allows(&Message::new("wgpu:any:message")));
        assert!(!filter.allows(&Message::new("wrong:subset:message")));
        assert!(filter.allows(&Message::new("another:subset:message")));
        assert!(filter.allows(&Message::new("other:subset:message:tail")));
    }
}
