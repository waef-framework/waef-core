pub mod builders;
pub mod commands;
pub mod component_query;
pub mod component_set;
pub mod extensions;
pub mod messages;
pub mod plugin;

use crate::core::WaefError;
use crate::util::pod::Pod;
use std::collections::HashMap;
pub trait IsComponent: Pod {
    fn component_name() -> &'static str {
        std::any::type_name::<Self>()
    }
    fn component_size() -> usize {
        std::mem::size_of::<Self>()
    }
}

pub trait EcsRegistry: Sync + Send {
    fn list_component_definitions(&self) -> HashMap<String, usize>;
    fn define_component_by_size(&self, name: String, size: usize) -> Result<(), WaefError>;

    fn get_component_size(&self, name: String) -> Option<usize>;

    fn create_entity(&self, entity_id: u128) -> Result<(), WaefError>;
    fn destroy_entity(&self, entity_id: u128) -> Result<(), WaefError>;

    fn create_entity_with_components(
        &self,
        entity_id: u128,
        components: HashMap<String, Box<[u8]>>,
    ) -> Result<(), WaefError> {
        self.create_entity(entity_id)?;
        for (component_name, component_buffer) in components {
            self.attach_component_buffer(component_name, entity_id, component_buffer)?;
        }
        Ok(())
    }

    fn attach_component_buffer(
        &self,
        component_name: String,
        entity_id: u128,
        component_buffer: Box<[u8]>,
    ) -> Result<(), WaefError>;
    fn detach_component_by_name(
        &self,
        component_name: String,
        entity_id: u128,
    ) -> Result<(), WaefError>;

    fn apply(&self, component_query: &[String], callback: &mut dyn FnMut(u128, &mut [&mut [u8]]));

    fn apply_set(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    );

    fn apply_single(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    );

    fn apply_until(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]) -> bool,
    ) {
        let mut escape = false;
        self.apply(component_query, &mut |id, components| {
            if !escape {
                escape = callback(id, components);
            }
        });
    }

    fn apply_set_unsafe(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    );
    fn apply_unsafe(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    );
    fn apply_single_unsafe(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    );
    fn apply_until_unsafe(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]) -> bool,
    ) {
        let mut escape = false;
        self.apply_unsafe(component_query, &mut |id, components| {
            if !escape {
                escape = callback(id, components);
            }
        });
    }

    fn apply_unsafe_batched(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(&[(u128, &[(*mut u8, usize)])]),
    ) {
        self.apply_unsafe(component_query, &mut |entity_id, items| {
            callback(&[(entity_id, items)])
        })
    }
    fn apply_set_unsafe_batched(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(&[(u128, &[(*mut u8, usize)])]),
    ) {
        self.apply_set_unsafe(entity_ids, component_query, &mut |entity_id, items| {
            callback(&[(entity_id, items)])
        })
    }
}
