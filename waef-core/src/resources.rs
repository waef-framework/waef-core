use std::hash::{DefaultHasher, Hash, Hasher};

pub mod extensions;

pub trait ResourceStore: Sync + Send {
    fn get_size(&self, key: u128) -> Option<usize>;
    fn get_cloned(&self, key: u128) -> Option<Vec<u8>>;

    fn remove(&self, key: u128) -> Option<Box<[u8]>>;
    fn get(&self, key: u128, buffer: &mut [u8]) -> Option<usize>;
    fn put(&self, key: u128, data: &[u8]);

    fn put_slice(&self, key: u128, start_index: usize, data: &[u8]);
    fn get_slice(&self, key: u128, start_index: usize, buffer: &mut [u8]) -> Option<usize>;
}

pub fn resource_id(category: &str, key: &str) -> u128 {
    let high = {
        let mut state = DefaultHasher::new();
        category.hash(&mut state);
        state.finish() as u128
    };
    let low = {
        let mut state = DefaultHasher::new();
        key.hash(&mut state);
        state.finish() as u128
    };
    high << 64 | low
}

#[cfg(test)]
mod resource_store_tests {
    #[test]
    pub fn to_resource_key_tests() {
        let aa = super::resource_id("some_category", "some_value_1");
        assert_eq!(aa, super::resource_id("some_category", "some_value_1"));

        let ab = super::resource_id("some_category", "some_value_2");
        assert_eq!(ab, super::resource_id("some_category", "some_value_2"));

        let ba = super::resource_id("othr_category", "some_value_1");
        assert_eq!(ba, super::resource_id("othr_category", "some_value_1"));

        let bb = super::resource_id("othr_category", "some_value_2");
        assert_eq!(bb, super::resource_id("othr_category", "some_value_2"));

        assert_ne!(aa, ab);
        assert_ne!(aa, ba);
        assert_ne!(aa, bb);
        assert_ne!(ab, ba);
        assert_ne!(ab, bb);
        assert_ne!(ba, bb);
    }
}
