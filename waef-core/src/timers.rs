pub mod messages;
pub mod tick;

use std::time::Duration;

use crate::DispatchFilter;

use super::core::{IsDisposable, IsTickable};

pub trait IsTimer: IsTickable + IsDisposable + Send {
    fn name(&self) -> String {
        "unknown".to_string()
    }

    fn duration(&self) -> Duration {
        Duration::from_millis(16)
    }

    fn outputs(&self) -> DispatchFilter {
        DispatchFilter::Any
    }
}

pub trait UseTimers {
    fn use_timer(self, actor: Box<dyn IsTimer>) -> Self;
}

pub trait TimersPlugin {
    fn apply<T: UseTimers>(self, target: T) -> T;
}

pub trait TimersInjector {
    fn inject<T: UseTimers>(&self, target: T) -> T;
}
