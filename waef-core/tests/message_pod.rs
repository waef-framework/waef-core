use waef_core::{message_pod, IsMessage};

#[message_pod("SomeMessage")]
struct SomeMessage {
    pub x: u32,
}

#[test]
pub fn test_some_message() {
    assert_eq!("SomeMessage", SomeMessage::category_name());

    let msg = SomeMessage { x: 100 };

    let roundtrip = SomeMessage::from_message(&msg.into_message()).unwrap();
    assert_eq!(msg.x, roundtrip.x);
}
