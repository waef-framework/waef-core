use serde::{Deserialize, Serialize};
use waef_core::{message_serde, IsMessage};

#[derive(Clone, Deserialize, Serialize)]
#[message_serde("SomeMessage")]
struct SomeMessage {
    pub i: u128,
    pub x: u32,
    pub items: Vec<u32>,
}

#[test]
pub fn test_some_message() {
    assert_eq!("SomeMessage", SomeMessage::category_name());

    let msg: SomeMessage = SomeMessage {
        i: 500400300200100000,
        x: 100,
        items: vec![200, 300, 400],
    };

    let roundtrip = SomeMessage::from_message(&msg.clone().into_message()).unwrap();
    assert_eq!(msg.i, roundtrip.i);
    assert_eq!(msg.x, roundtrip.x);
    assert_eq!(msg.items, roundtrip.items);
}
