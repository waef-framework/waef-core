use waef_core::component;
use waef_core::IsComponent;

#[component("SomeComponentName")]
struct SomeComponent {
    _x: u32,
}

#[test]
pub fn test_some_component() {
    assert_eq!("SomeComponentName", SomeComponent::component_name());
}
