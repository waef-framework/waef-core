use waef_core::Pod;

#[repr(C)]
#[derive(Clone, Copy, Pod)]
struct TestPod {
    pub x: u32,
}

#[test]
pub fn test_derive_pod() {
    let p: TestPod = Pod::from_bytes(&[0, 0, 0, 0]);

    assert_eq!(0, p.x);
}

/* fails */
/*
#[derive(Clone, Copy)]
struct IsNotPod {
    x: u32
}

#[derive(Clone, Copy, Pod)]
struct TestFailingPod {
    pub x: IsNotPod,
}
*/
